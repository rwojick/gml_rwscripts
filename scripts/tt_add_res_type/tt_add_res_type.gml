/// @func tt_add_res_type(type_name) = success;
/// @desc Tag Text - Add Resource Type
/// @arg type_name
/// @return success

var name = string(argument[0]);

var type = undefined;

if (!is_undefined(global.tt_res_types[? name]))
{
	return (false);	
}

type = ds_map_create();

global.tt_res_types[? name] = type;

return (true);
