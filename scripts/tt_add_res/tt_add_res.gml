/// @func tt_add_res(resource_type, resource_name, resource_value) = success;
/// @desc Tag Text - Add Resource
/// @arg resource_type
/// @arg resource_name
/// @arg resource_id
/// @return success

var type = string(argument[0]),
	name = string(argument[1]),
	res = argument[2];

var res_type = global.tt_res_types[? type];

if (is_undefined(res_type))
{
	return (false);	
}

res_type[? name] = res;

return (true);
