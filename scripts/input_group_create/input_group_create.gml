/// @desc Input System - Create Group
/// @arg group_name
/// @arg action_name
/// @arg *action_name
/// @return success

var name = argument[0];

if (is_undefined(input_group[? name]))
{
	var list = undefined, // Array
		c1 = 1;

	repeat (argument_count - 1)
	{
		list[c1 - 1] = argument[c1];
		
		c1++;
	}
	
	input_group[? name] = list;
	
    return (true);
}

return (false);
