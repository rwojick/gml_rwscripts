/// @desc Input System - Enable/Disable Gamepad
/// @arg *enabled_status

var enable = argument_count >= 1 ? argument[0] : true;

input_gp_enabled = enable;

if (enable) and (input_gp_supported)
{
	input_gp_get_connected();
}
