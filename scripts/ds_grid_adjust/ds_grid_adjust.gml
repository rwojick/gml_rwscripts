/// @desc DS Grid Adjust
/// @arg ds_grid
/// @arg num_rows
/// @arg num_cols

// Note:  Resizes the grid relative to the values passed to row/col
// Eg:  ds_grid_adjust(grid, 1, 1) - Adds a new row and a new column
// Eg:  ds_grid_adjust(grid, 5, 0) - Adds 5 new rows, but no columns
// Eg:  ds_grid_adjust(grid, -1, 0) - Removes one row

// Input Vars
var grid_id = argument[0];

if (ds_exists(grid_id, ds_type_grid))
{
    var rows = argument[0],
        cols = argument[1];
    
    ds_grid_resize(grid_id, ds_grid_width(grid_id) + cols, ds_grid_height(grid_id) + rows);
    
    return (true);
}

return (false);
