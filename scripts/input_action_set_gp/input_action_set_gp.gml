/// @desc Input System - Set Action Trigger Gamepad
/// @arg action_name
/// @arg gp_type
/// @arg gp_value
/// @return success

// Inputs
var name = string(argument[0]),
	type = argument[1],
	val = argument[2];

if (!is_undefined(input_action[? name]))
{
	var action = input_action[? name];
	
	action[? "trig_gp_type"] = type;
	action[? "trig_gp_val"] = val;
	
	return (true);
}

return (false);
