#region Declarations
	var data = argument[0];
	
	var tag = undefined,
		type = global.tt_tags[? data[0]];
#endregion

#region Main
	switch (data[0])
	{
		case tt_icon_char:
			tag[tt_node.type] = tt_icon_char;
			tag[tt_node.data] = tt_icon;
			tag[tt_node.w1] = string_width(tt_icon);
			tag[tt_node.h1] = string_height(tt_icon);
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			break;			
		case tt_tag_icon:
			tag[tt_node.type] = tt_tag_icon;
			draw_set_font(tt_get_res(tt_rt_font, tt_font_icon));
			tag[tt_node.data] = data[1];
			tag[tt_node.w1] = string_width(data[1]);
			tag[tt_node.h1] = string_height(data[1]);
			draw_set_font(global.tt_font);
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			break;
		case tt_color:
			var col = tt_get_res(tt_rt_color, data[1])
			
			tag[tt_node.type] = tt_color;
			tag[tt_node.data] = is_undefined(col) ? tt_get_res(tt_rt_color, tt_col_def) : col;
			tag[tt_node.w1] = 0;
			tag[tt_node.h1] = 0;
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			break;
		case tt_rgb:
			tag[tt_node.type] = tt_rgb;
			tag[tt_node.data] = make_color_rgb(real(data[1]), real(data[2]), real(data[3]));
			tag[tt_node.w1] = 0;
			tag[tt_node.h1] = 0;
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			break;
		case tt_tag_font:
			tag[tt_node.type] = tt_tag_font;
			tag[tt_node.data] = is_undefined(tt_get_res(tt_rt_font, data[1])) ? tt_font_def : data[1];
			tag[tt_node.w1] = 0;
			tag[tt_node.h1] = 0;
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			
			tt_set_font(tag[tt_node.data]);
			break;
		case tt_var:
			var val = tt_get_res(tt_rt_var, data[1]);
			
			val = is_undefined(val) ? "" : string(val);
			val = string_replace_all(val, "\n", "");
			
			tag[tt_node.type] = tt_var;
			tag[tt_node.data] = val;
			tag[tt_node.w1] = string_width(val);
			tag[tt_node.h1] = string_height(val);
			tag[tt_node.len] = string_length(val);
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			break;
		case tt_def:
			tag[tt_node.type] = tt_def;
			tag[tt_node.data] = tt_get_res(tt_rt_color, tt_col_def);
			tag[tt_node.w1] = 0;
			tag[tt_node.h1] = 0;
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			break;
		case tt_reset:
			tag[tt_node.type] = tt_reset;
			tag[tt_node.data] = [ tt_get_res(tt_rt_color, tt_col_def), tt_font_def ];
			tag[tt_node.w1] = 0;
			tag[tt_node.h1] = 0;
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			tt_set_font(tt_font_def);
			break;
		default:
			// Shouldn't ever happen, but just in case...
			tag = undefined;
			break;
	}
	
	return (tag);
#endregion
