/// @desc Input System - Get Gamepad Trigger Name
/// @arg gp_type
/// @arg gp_value
/// @return input_gp_name

var gp_type = argument[0],
	gp_val = argument[1];

var gp_code = string(gp_type) + ":" + string(gp_val),
	name = input_gp_name[? gp_code];

name = is_undefined(name)? "Gamepad Code " + gp_code : name;

return (name);
