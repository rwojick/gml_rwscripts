/// @desc Input System - Is Action Active?
/// @arg action_name
/// @return is_active

var name = string(argument[0]);

if (!is_undefined(input_action[? name]))
{
	var index = ds_list_find_index(input_active, name);
	
	return (ds_list_find_index(input_active, name) != -1);
}

return (false);
