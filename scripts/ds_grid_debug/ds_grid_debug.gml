/// @desc DS Grid to Debug String
/// @arg ds_grid

// Input Vars
var grid = argument[0];

// Script Vars
var str = "{ ";

if (ds_exists(grid, ds_type_grid))
{
    var x1 = 0,
        y1 = 0;

    repeat (ds_grid_height(grid))
    {
        str += "{ ";
        
        repeat (ds_grid_width(grid))
        {
            str += string(grid[# x1, y1]) + ", ";
            
            x1++;
        }
        
        str += "}, ";
        
        x1 = 0;
        y1++;
    }
}

str += " }";

return (str);
