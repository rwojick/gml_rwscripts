/// @desc Input System - Get Action Value
/// @arg action_name
/// @return action_value or undefined

if (instance_exists(global.input_id))
{
    with (global.input_id)
    {
        var name = argument[0];
        
        if (!is_undefined(input_action[? name]))
        {
            return (input_value[? name]);
        }
    }
}

return (undefined);
