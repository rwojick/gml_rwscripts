/// @func tt_ext_def_init() = success;
/// @desc Tag Text - Default Ext. Initialize Event
/// @return success

#macro tt_word "word"
#macro tt_space "space"
#macro tt_newline "newline"
#macro tt_tag_open "open"
#macro tt_font_def "default"

tt_add_res("font", tt_font_def, noone);

/* Tags:
word:		text
space:		n/a
newline:	n/a
open:		n/a
*/

/* PreParse Features (Using default chars)
\n:		New Line
\[:		Tag Open Char
*/
