/// @desc Index to Point
/// @arg index
/// @arg width
/// @return point_array

var index = argument[0],
	w1 = argument[1];
	
var point = [];

point[0] = index mod w1;
point[1] = index div w1;

return (point);
