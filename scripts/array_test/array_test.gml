var r1 = 10000,
	n1 = 50,
	n2 = 50,
	c1 = 0,
	a1 = [],
	a2 = [],
	c2 = 0,
	c3 = 0;

repeat (n1 * n2)
{
	a1[c1] = c1;
	c1++;
}

c1 = 0;

repeat (n1)
{
	var arr = [];
		
	repeat (n2)
	{
		
		arr[c2] = c3;
		c2++;
		c3++;
	}

	a2[c1] = arr;
	c2 = 0;
	c1++;
}

var t1 = 0,
	t2 = 0,
	sum = 0;

c1 = 0;
c2 = 0;

msg("Linear Array Test");

t1 = get_timer();

repeat (r1)
{
	c1 = 0;
	sum = 0;
	
	repeat (n1 * n2)
	{
		sum += a1[@ c1];
		c1++;
	}
}

t1 = get_timer() - t1;

c1 = 0;

msg("Nested array Test");

t2 = get_timer();

repeat (r1)
{
	c1 = 0;
	c2 = 0;
	sum = 0;
	
	repeat (n1)
	{
		var arr = a2[@ c1];
		
		repeat (n2)
		{
			sum += arr[@ c2];
			c2++;
		}
		
		c2 = 0;
		c1++;
	}
}

t2 = get_timer() - t2;

msg("Linear: " + string(t1) +"\nNested: " + string(t2));
