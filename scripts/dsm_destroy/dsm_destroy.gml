/// @desp DSM - Destroy

// Script Vars
var key = ds_map_find_first(global.dsm_group);

repeat (ds_map_size(global.dsm_group))
{
	dsm_group_free(key);
	
	key = ds_map_find_next(global.dsm_group, key);
}

ds_map_destroy(global.dsm_group[? global.dsm_default]);

ds_map_destroy(global.dsm_group);
