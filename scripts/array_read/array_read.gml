/// @desc Array Read from String
/// @arg string

// Inputs
var str = argument[0];

// Output
var array = [];

// Script Vars
var temp = ds_list_create();

ds_list_read(temp, str);

array = temp[| 0];

ds_list_destroy(temp);

if (!is_array(array))
{
	return ([]);
}

return (array);
