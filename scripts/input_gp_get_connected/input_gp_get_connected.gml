/// @desc Input System - Get Connected Gamepad IDs

var c1 = 0;

repeat (input_gp_num_devices)
{
	input_gp_connected[c1] = gamepad_is_connected(c1);
	c1++;	
}
