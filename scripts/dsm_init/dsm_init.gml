/// @desc DSM - Initialize

// Setup macros + Enums
#macro dsm_debug true

enum dsm_type
{
	none = -1,
	map,
	grid,
	list,
	queue,
	priority
}

// Setup global values
global.dsm_group = ds_map_create(); // Stores DS Lists that contain 2 index arrays with type:id
global.dsm_default = "__default"; // Default group name, for simpler projects

global.dsm_group[? global.dsm_default] = ds_list_create();
