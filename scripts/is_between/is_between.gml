/// @desc Is Between
/// @arg value
/// @arg min_value
/// @arg max_value

// Inputs
var val = argument[0],
	lo = min(argument[1], argument[2]),
	hi = max(argument[1], argument[2]);

return (clamp(val, lo, hi) == val);
