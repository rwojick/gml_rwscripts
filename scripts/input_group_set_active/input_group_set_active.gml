/// @desc Input System - Set Group Active State
/// @arg name
/// @arg *active
/// @return success

// Inputs
var name = string(argument[0]),
	active = argument_count >= 2 ? argument[1] : true;

if (!is_undefined(input_group[? name]))
{
	var group = input_group[? name],
		c1 = 0;
		
	repeat (array_length_1d(group))
	{
		var index = ds_list_find_index(input_active, group[@ c1]);
		
		if (index == -1) and (active)
		{
			ds_list_add(input_active, group[@ c1]);
		}
		else
		{
			if (index != -1) and (!active)
			{
				ds_list_delete(input_active, index);
				
				input_value[? group[@ c1]] = 0;
			}
		}
		
		c1++;
	}
	
	return (true);
}

return (false);
