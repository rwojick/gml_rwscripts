/// @desc String Split Extended
/// @arg string
/// @arg *deliminator

// Inputs
var str = string(argument[0]),
	delim = argument_count >= 2 ? string(argument[1]) : "|";

// Script Vars
var arr = [],
    c1 = 0,
    p1 = 0;

if (str == "") or (delim = "")
{
    return ([]);
}

do
{
    var temp = "";
    
    p1 = string_pos(delim, str);
    
    if (p1 == 0)
    {
        temp = str;
    }
    else
    {
        temp = string_copy(str, 1, p1 - 1);
        
        str = string_delete(str, 1, p1);
    }
    
    if (string_char_at(temp, 1) == "$")
    {
        temp = string_delete(temp, 1, 1);
    }
    else
    {
        temp = real(temp);
    }
        
    arr[c1++] = temp;
} until (p1 == 0)

return (arr);
