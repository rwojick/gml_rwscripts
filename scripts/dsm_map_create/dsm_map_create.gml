/// @desc DSM - Create Map [map_id]
/// @arg *group_name

// Inputs
var name = argument_count >= 1 ? argument[0] : global.dsm_default;

// Function Vars
var map = ds_map_create(),
	group = global.dsm_group[? name],
	ds = undefined;

if (is_undefined(group))
{
	if (dsm_debug)
	{
		show_error("DSM - Create Map, Group does not exist: " + name, false);
		
		return (undefined);
	}
}

ds[0] = dsm_type.map;
ds[1] = map;

ds_list_add(group, ds);

return (map);
