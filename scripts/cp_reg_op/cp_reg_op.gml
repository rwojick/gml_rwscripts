/// @func cp_reg_op(operator, script_id, presidence, *association) = success;
/// @desc Code Parser - Register Operator
/// @arg operator
/// @arg script_id
/// @arg presidence
/// @arg *association
/// @return success

var op = argument[0],
	scr = argument[1],
	pres = argument[2],
	assoc = (argument_count >= 4) ? argument[3] : op_assoc.left;

if (!is_undefined(cp_ops[? op]))
{
	return (false);
}

cp_ops[? op] = [scr, pres, assoc];

return (true);
