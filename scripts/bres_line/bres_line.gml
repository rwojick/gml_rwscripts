/// @func bres_line(x1, y1, x2, y1) = line_array;
/// @desc Bresenham's Line
/// @arg x1
/// @arg y1
/// @arg x2
/// @arg y2
/// @return line_array

var x1 = argument[0],
	y1 = argument[1],
	x2 = argument[2],
	y2 = argument[3];

var dx = abs(x2 - x1),
	sx = (x1 < x2) ? 1 : -1,
	dy = abs(y2 - y1),
	sy = (y1 < y2) ? 1 : -1,
	err = ((dx > dy) ? dx : -dy) / 2,
	e2 = 0,
	done = false,
	bline = [],
	c1 = 0;

do
{
	bline[c1] = [x1, y1];
	c1++;
	
	if (x1 == x2) && (y1 == y2)
	{
		done = true;
	}
	else
	{
		e2 = err;
		
		if (e2 > -dx)
		{
			err -= dy;
			x1 += sx;
		}
		
		if (e2 < dy)
		{
			err += dx;
			y1 += sy;
		}
	}
} until (done);

return (bline);

/*
var dx = Math.abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
  var dy = Math.abs(y1 - y0), sy = y0 < y1 ? 1 : -1; 
  var err = (dx>dy ? dx : -dy)/2;
 
  while (true) {
    setPixel(x0,y0);
    if (x0 === x1 && y0 === y1) break;
    var e2 = err;
    if (e2 > -dx) { err -= dy; x0 += sx; }
    if (e2 < dy) { err += dx; y0 += sy; }
  }
}
*/
