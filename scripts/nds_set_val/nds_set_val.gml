/// @desc NDS Control - Set Value
/// @arg nds_id
/// @arg path
/// @arg value
/// @return success

var nds = argument[0],
	path = string(argument[1]),
	val = argument[2];

var data = nds,
	index_list = string_split(path, nds_delim),
	len = array_length_1d(index_list),
	i = 0,
	prev_data = undefined;

repeat (len - 1) // The last index will be what the value is applied to
{
	var index = index_list[i],
		type = string_char_at(index, 1);
	
	switch (type)
	{
		case nds_list_key:
			index = real(string_delete(index, 1, 1));
			
			// Create data structure if needed
			if (is_undefined(data)) or (!ds_exists(data, ds_type_list))
			{
				var data = ds_list_create();
				
				nds_add_ds(data, prev_data);
			}
			
			// Create index if needed
			if (index >= ds_list_size(data))
			{
				data[| index] = undefined;
			}
			
			// Set last data
			prev_data[0] = nds_type.list;
			prev_data[1] = data;
			prev_data[2] = index;
				
			// Get data
			data = data[| index];
			break;
		case nds_grid_key:
			index = string_delete(index, 1, 1);
			index = string_split_ext(index, ",");
			
			// Create data structure if needed
			if (is_undefined(data)) or (!ds_exists(data, ds_type_grid))
			{
				var data = ds_grid_create(1, 1);
				
				nds_add_ds(data, prev_data);
			}
			
			// Create index if needed
			if (index[1] >= ds_grid_width(data) or (index[0] >= ds_grid_height(data)))
			{
				var w1 = ds_grid_width(data),
					h1 = ds_grid_height(data);
				
				ds_grid_resize(data, max(index[1] + 1, w1), max(index[0] + 1, h1));
			}
			
			// Set last data
			prev_data[0] = nds_type.grid;
			prev_data[1] = data;
			prev_data[2] = index;
				
			// Get data
			data = data[# index[1], index[0]];
			break;
		case nds_array1d_key:
			index = real(string_delete(index, 1, 1));
			
			// Create data structure if needed
			if (!is_array(data))
			{
				var data = [];
				
				nds_add_ds(data, prev_data);
			}
			
			// Create index if needed
			if (index >= array_length_1d(data))
			{
				data[@ index] = undefined;
			}
			
			// Set last data
			prev_data[0] = nds_type.array1d;
			prev_data[1] = data;
			prev_data[2] = index;
				
			// Get data
			data = data[@ index];
			break;
		case nds_array2d_key:
			index = string_delete(index, 1, 1);
			index = string_split_ext(index, ",");
			
			// Create data structure if needed
			if (!is_array(data))
			{
				var data = undefined;
				
				data[0, 0] = undefined;
				
				nds_add_ds(data, prev_data);
			}
			
			// Create index if needed
			// First check height
			if (index[0] >= array_height_2d(data))
			{
				data[@ index[0], 0] = undefined;
			}
			
			if (index[1] >= array_length_2d(data, index[0]))
			{
					data[@ index[0], index[1]] = undefined;
			}
			
			// Set last data
			prev_data[0] = nds_type.array2d;
			prev_data[1] = data;
			prev_data[2] = index;
				
			// Get data
			data = data[@ index[0], index[1]];
			break;
		case nds_map_key:
			index = string_delete(index, 1, 1);
		default:
			// Create data structure if needed
			if (is_undefined(data)) or (!ds_exists(data, ds_type_map))
			{
				var data = ds_map_create();
				
				nds_add_ds(data, prev_data);
			}
			
			// Create index if needed
			if (is_undefined(data[? index]))
			{
				data[? index] = undefined;
			}
			
			// Set last data
			prev_data[0] = nds_type.map;
			prev_data[1] = data;
			prev_data[2] = index;
				
			// Get data
			data = data[? index];
			break;
	}
	
	i++;
}

// NOTE:  NEED TO APPLY CHANGES ABOVE TO BELOW SECTION!

// Found path.  Apply value
index = index_list[i];
type = string_char_at(index, 1);

switch (type)
{
	case nds_list_key:
		index = real(string_delete(index, 1, 1));
			
		// Create data structure if needed
		if (is_undefined(data)) or (!ds_exists(data, ds_type_list))
		{
			var data = ds_list_create();
				
			nds_add_ds(data, prev_data);
		}
			
		// Create index if needed
		if (index >= ds_list_size(data))
		{
			data[| index] = undefined;
		}
		
		data[| index] = val;
		break;
	case nds_grid_key:
		index = string_delete(index, 1, 1);
		index = string_split_ext(index, ",");
		
		// Create data structure if needed
		if (is_undefined(data)) or (!ds_exists(data, ds_type_grid))
		{
			var data = ds_grid_create(1, 1);
				
			nds_add_ds(data, prev_data);
		}
			
		// Create index if needed
		if (index[1] >= ds_grid_width(data) or (index[0] >= ds_grid_height(data)))
		{
			var w1 = ds_grid_width(data),
				h1 = ds_grid_height(data);
				
			ds_grid_resize(data, max(index[1] + 1, w1), max(index[0] + 1, h1));
		}
		
		data[# index[1], index[0]] = val;
		break;
	case nds_array1d_key:
		index = real(string_delete(index, 1, 1));
		
		// Create data structure if needed
		if (!is_array(data))
		{
			var data = [];
				
			nds_add_ds(data, prev_data);
		}
			
		// Create index if needed
		if (index >= array_length_1d(data))
		{
			data[@ index] = undefined;
		}
		
		data[@ index] = val;
		break;
	case nds_array2d_key:
		index = string_delete(index, 1, 1);
		index = string_split_ext(index, ",");
			
		// Create data structure if needed
		if (!is_array(data))
		{
			var data = undefined;
				
			data[0, 0] = undefined;
				
			nds_add_ds(data, prev_data);
		}
			
		// Create index if needed
		// First check height
		if (index[0] >= array_height_2d(data))
		{
			data[@ index[0], 0] = undefined;
		}
			
		if (index[1] >= array_length_2d(data, index[0]))
		{
				data[@ index[0], index[1]] = undefined;
		}
			
		data[@ index[0], index[1]] = val;
		break;
	case nds_map_key:
		index = string_delete(index, 1, 1);
	default:
		// Create data structure if needed
		if (is_undefined(data)) or (!ds_exists(data, ds_type_map))
		{
			var data = ds_map_create();
				
			nds_add_ds(data, prev_data);
		}
			
		// Create index if needed
		if (is_undefined(data[? index]))
		{
			data[? index] = undefined;
		}
		
		// Set the final value
		data[? index] = val;
		break;
}

return (true);
