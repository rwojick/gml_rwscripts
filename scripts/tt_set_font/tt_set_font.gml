/// @func tt_set_font(font_name) = success;
/// @desc Tag Text - Set Draw Font
/// @arg font_name
/// @return success

var name = string(argument[0]);

var font = tt_get_res(tt_rt_font, name);

if (is_undefined(font))
{
	return (false);
}

draw_set_font(font);
global.tt_font = font;

return (true);
