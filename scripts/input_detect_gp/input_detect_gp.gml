/// @desc Input System - Detect Gamepad Trigger
/// @return gp_data

var gp_type = undefined,
    gp_val = undefined;

if (input_gp_id != input_gp_disabled)
{
    gp_type = input_gp_type.button;
            
    if (gamepad_button_check(input_gp_id, gp_face1))
    {
        gp_val = gp_face1;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_face2))
    {
        gp_val = gp_face2;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_face3))
    {
        gp_val = gp_face3;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_face4))
    {
        gp_val = gp_face4;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_padd))
    {
        gp_val = gp_padd;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_padu))
    {
        gp_val = gp_padu;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_padl))
    {
        gp_val = gp_padl;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_padr))
    {
        gp_val = gp_padr;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_select))
    {
        gp_val = gp_select;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_shoulderl))
    {
        gp_val = gp_shoulderl;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_shoulderr))
    {
        gp_val = gp_shoulderr;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_shoulderlb))
    {
        gp_val = gp_shoulderlb;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_shoulderrb))
    {
        gp_val = gp_shoulderrb;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_start))
    {
        gp_val = gp_start;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_stickl))
    {
        gp_val = gp_stickl;
		return ([gp_type, gp_val]);
    }
        
    if (gamepad_button_check(input_gp_id, gp_stickr))
    {
        gp_val = gp_stickr;
		return ([gp_type, gp_val]);
    }
        
    dir = sign(gamepad_axis_value(input_gp_id, gp_axislh)) * round(abs(gamepad_axis_value(input_gp_id, gp_axislh)));
        
    if (dir != 0)
    {
        gp_type = input_gp_type.axis_lh;
        gp_val = dir;
		return ([gp_type, gp_val]);
    }
        
    dir = sign(gamepad_axis_value(input_gp_id, gp_axislv)) * round(abs(gamepad_axis_value(input_gp_id, gp_axislv)));
        
    if (dir != 0)
    {
        gp_type = input_gp_type.axis_lv;
        gp_val = dir;
		return ([gp_type, gp_val]);
    }
        
    dir = sign(gamepad_axis_value(input_gp_id, gp_axisrh)) * round(abs(gamepad_axis_value(input_gp_id, gp_axisrh)));
        
    if (dir != 0)
    {
        gp_type = input_gp_type.axis_rh;
        gp_val = dir;
		return ([gp_type, gp_val]);
    }
        
    dir = sign(gamepad_axis_value(input_gp_id, gp_axisrv)) * round(abs(gamepad_axis_value(input_gp_id, gp_axisrv)));
        
    if (dir != 0)
    {
        gp_type = input_gp_type.axis_rv;
        gp_val = dir;
		return ([gp_type, gp_val]);
    }
}

return (undefined);
