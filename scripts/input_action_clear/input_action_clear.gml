/// @desc Input System - Clear Action
/// @arg action_name
/// @return success

var name = string(argument[0]);

if (!is_undefined(input_action[? name]))
{
    input_value[? name] = 0;
    
    return (true);
}

return (false);
