/// @desc Draw Text with Drop Shadow
/// @arg x
/// @arg y
/// @arg text
/// @arg *color
/// @arg *offset

// inputs
var x1 = argument[0],
    y1 = argument[1],
    text = string(argument[2]),
	offset = argument_count >= 4 ? argument[3] : 1,
	col = argument_count >= 5 ? argument[4] : c_black;

// Script Vars
var curr_col = draw_get_color();

draw_set_color(col);

draw_text(x1 + offset, y1 + offset, text);

draw_set_color(curr_col);

draw_text(x1, y1, text);
