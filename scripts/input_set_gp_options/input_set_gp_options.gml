/// @desc Input System - Set Gamepad Settings
/// @arg axis_deadzone
/// @arg axis_threshold
/// @arg button_threshold

var axis_dz = argument[0],
	axis_th = argument[1],
	but_th = argument[2];

input_gp_axis_deadzone = axis_dz;
input_gp_axis_threshold = axis_th;
input_gp_button_threshold = but_th;
