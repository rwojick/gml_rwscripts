/// @desc Input System - Set Action Active State
/// @arg action_name
/// @arg *active
/// @return success

var name = string(argument[0]),
	active = argument_count >= 2 ? argument[1] : true;

if (!is_undefined(input_action[? name]))
{
	var index = ds_list_find_index(input_active, name);
		
	if (index == -1) and (active)
	{
		ds_list_add(input_active, name);
	}
	else
	{
		if (index != -1) and (!active)
		{
			ds_list_delete(input_active, index);
				
			input_value[? name] = 0;
		}
	}
	
	return (true);
}

return (false);
