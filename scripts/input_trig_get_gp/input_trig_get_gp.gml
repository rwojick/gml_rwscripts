/// @desc Input System - Get Gamepad Triggered
/// @arg action_name
/// @return trigger_state

var name = argument[0];

if (input_gp_id == input_gp_disabled) or (!input_gp_enabled) or (!input_gp_supported)
{
	return (false);
}

var action = input_action[? name];

switch (action[? "trig_gp_type"])
{
	case input_gp_type.button:
		return (gamepad_button_check(input_gp_id, action[? "trig_gp_val"]));
	case input_gp_type.axis_lh:
		var axis = gamepad_axis_value(input_gp_id, gp_axislh);
		
		return ((sign(axis) == sign(action[? "trig_gp_val"])) and (abs(action[? "trig_gp_val"]) >= input_gp_axis_threshold));
	case input_gp_type.axis_lv:
		var axis = gamepad_axis_value(input_gp_id, gp_axislv);
		
		return ((sign(axis) == sign(action[? "trig_gp_val"])) and (abs(action[? "trig_gp_val"]) >= input_gp_axis_threshold));
	case input_gp_type.axis_rh:
		var axis = gamepad_axis_value(input_gp_id, gp_axisrh);
		
		return ((sign(axis) == sign(action[? "trig_gp_val"])) and (abs(action[? "trig_gp_val"]) >= input_gp_axis_threshold));
	case input_gp_type.axis_rv:
		var axis = gamepad_axis_value(input_gp_id, gp_axisrv);
		
		return ((sign(axis) == sign(action[? "trig_gp_val"])) and (abs(action[? "trig_gp_val"]) >= input_gp_axis_threshold));
	case input_gp_type.disabled:
	default:
		return (false);
}

// Failsafe.
return (undefined);
