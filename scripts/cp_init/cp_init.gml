/// @func cp_init();
/// @desc Code Parser - Initialize

enum token_typeeee
{
    none = -1,
    lf_par,
    rt_par,
    comma,
    op,
    func,
    vari,	// Basic variables
    term,
    num,
    str,
	rpn,	// Reference to a new RPN layer
	ds		// Also includes arrays
}

enum op_index
{
	scr,
	pres,
	assoc
}

enum op_assoc
{
	none = -1,
	left,
	right
}

cp_ops = ds_map_create();
cp_func = ds_map_create();
cp_var = ds_map_create();	// System specific variables, this is checked first before relying on global/instance vars

cp_reg_op("-:", cp_ops_def, 2, op_assoc.right);	// Negation, ie: 10 + (-5)
cp_reg_op("*", cp_ops_def, 3);
cp_reg_op("/", cp_ops_def, 3);
cp_reg_op("\\", cp_ops_def, 3);
cp_reg_op("%", cp_ops_def, 3);
cp_reg_op("+", cp_ops_def, 4);
cp_reg_op("-", cp_ops_def, 4);
cp_reg_op("<", cp_ops_def, 6);
cp_reg_op(">", cp_ops_def, 6);
cp_reg_op("<=", cp_ops_def, 6);
cp_reg_op(">=", cp_ops_def, 6);
cp_reg_op("==", cp_ops_def, 7);
cp_reg_op("!=", cp_ops_def, 7);
cp_reg_op("&&", cp_ops_def, 11);
cp_reg_op("^^", cp_ops_def, 11.5);
cp_reg_op("||", cp_ops_def, 12);
cp_reg_op("=", cp_ops_def, 14, op_assoc.right);
cp_reg_op("+=", cp_ops_def, 14, op_assoc.right);
cp_reg_op("-=", cp_ops_def, 14, op_assoc.right);
cp_reg_op("*=", cp_ops_def, 14, op_assoc.right);
cp_reg_op("/=", cp_ops_def, 14, op_assoc.right);
cp_reg_op("\\=", cp_ops_def, 14, op_assoc.right);
cp_reg_op("%=", cp_ops_def, 14, op_assoc.right);
