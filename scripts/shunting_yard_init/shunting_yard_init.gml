/// @description  shunting_yard_init();
/// @function  shunting_yard_init

enum token_type
{
    none = -1,
    left_paren,
    right_paren,
    comma,
    op,
    func,
    variable,
    term,
    num,
    str
}

var ops = "+ 4 0 - 4 0 * 3 0 / 3 0 \\ 3 0 % 3 0 = 14 1 == 7 0 += 14 1 -= 14 1 *= 14 1 /= 14 1 \\= 14 1 %= 14 1 != 7 0 ";
ops += "< 6 0 > 6 0 <= 6 0 >= 6 0 && 11 0 || 12 0 ^^ 11.5 0 -: 2 1";

ops = string_split(ops, " ");

var c1 = 0,
    len = array_length_1d(ops) / 3;

shyrd_ops = ds_grid_create(3, len);

repeat (len)
{
    shyrd_ops[# 0, c1] = ops[(c1 * 3)];
    shyrd_ops[# 1, c1] = real(ops[(c1 * 3) + 1]);
    shyrd_ops[# 2, c1++] = real(ops[(c1 * 3) + 2]);
}

shyrd_funcs = ds_list_create();

var funcs = "null_func";

funcs = string_split(funcs);

len = array_length_1d(funcs);

c1 = 0;

repeat (len)
{
    shyrd_funcs[| c1] = funcs[c1];
    c1++;
}
