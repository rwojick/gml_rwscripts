/// @desc Input System - Get Gamepad Enabled/Disabled
/// @return enabled_status

return (input_gp_enabled);
