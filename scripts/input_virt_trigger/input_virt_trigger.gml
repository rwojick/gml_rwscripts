/// @desc Input System - Trigger Virtual Input
/// @arg virtual_input_name
/// @return success

if (instance_exists(global.input_id))
{
    with (global.input_id)
    {
		var name = argument[0];
		
        input_virts[? name] = 0;
    
        return (true);
    }
}

return (false);
