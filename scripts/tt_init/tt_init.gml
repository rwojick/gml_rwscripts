/// @func tt_init() = success;
/// @desc Tag Text - Initialize
/// @return success

#region Macros and Enums
	#macro tt_open "["
	#macro tt_close "]"
	#macro tt_escape "\\"
	#macro tt_rt_font "font"
	
	enum tt_node
	{
		type,
		data,
		w1,
		h1,
		len,
		is_wb,
		is_nl,
		draw
	}
	
	enum tt_alignment
	{
		left,
		center,
		right
	}
	
	enum tt_index
	{
		nodes,
		w1,
		h1,
		len
	}
#endregion

#region Declarations
	// Globals
	global.tt_tags = ds_map_create();
	global.tt_exts = ds_map_create();
	global.tt_res_types = ds_map_create();
	
	global.tt_font = undefined;
	global.tt_align = tt_alignment.left;
	global.tt_line_h1 = 1.0;
#endregion

#region Main
	tt_add_res_type(tt_rt_font);
	tt_add_ext("default", tt_ext_def_init, tt_ext_def_pre, tt_ext_def_parse, tt_ext_def_draw, noone, tt_word, tt_space, tt_newline, tt_tag_open);
	tt_add_ext("basic_text", tt_ext_bt_init, tt_ext_bt_pre, tt_ext_bt_parse, tt_ext_bt_draw, tt_ext_bt_destroy, tt_icon_char, tt_tag_icon, tt_color, tt_rgb, tt_tag_font, tt_reset, tt_def, tt_var);
#endregion

return (true);
