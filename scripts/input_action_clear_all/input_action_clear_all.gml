/// @desc Input System - Clear All Actions

var key = ds_map_find_first(input_value);

repeat (ds_map_size(input_value))
{
    input_value[? key] = 0;
    
    key = ds_map_find_next(input_value, key);
}
