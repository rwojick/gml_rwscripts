/// @desc String Split
/// @arg string
/// @arg *deliminator

// Inputs
var str = string(argument[0]),
	delim = (argument_count >= 2) ? string(argument[1]) : "|";

// Output
var array = [];

// Script Vars
var p1 = 0,
	c1 = 0,
	dlen = string_length(delim);

if (str == "") or (delim == "")
{
	return ([]);
}

p1 = string_pos(delim, str);

if (p1 == 0)
{
	array[0] = str;
	
	return (array);
}

while (p1 != 0)
{
	var data = string_copy(str, 1, p1 - 1);
	
	array[c1++] = data;
	
	str = string_delete(str, 1, p1 + dlen - 1);
	
	p1 = string_pos(delim, str);
}

array[c1] = str;

return (array);
