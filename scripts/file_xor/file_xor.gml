/// @func file_xor();

var fn = get_open_filename("all files|*.*", ""),
	file = file_bin_open(fn, 0),
	new = file_bin_open(fn + ".new", 1),
	c1 = 0;

repeat (file_bin_size(file))
{
	var val = file_bin_read_byte(file);
	
	file_bin_write_byte(new, val ^ $ff)
}

file_bin_close(file);
file_bin_close(new);

msg("Done~!");
