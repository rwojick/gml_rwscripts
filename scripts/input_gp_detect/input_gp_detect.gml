/// @desc Input System - Detect Gamepad
// Note:  Needs to be in Event: Async -> System Event

if (input_gp_supported) and (input_gp_enabled)
{
    switch(async_load[? "event_type"])
    {
        case "gamepad discovered":
			input_gp_connected[async_load[? "pad_index"]] = true;
            break;
        case "gamepad lost":
			input_gp_connected[async_load[? "pad_index"]] = false;
            break;
    }
}
