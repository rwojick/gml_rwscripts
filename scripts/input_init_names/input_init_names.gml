/// @desc Input System - Initialize Trigger Names

// Keyboard Names
input_key_name[? string(vk_left)] = "Left Arrow";
input_key_name[? string(vk_right)] = "Right Arrow";
input_key_name[? string(vk_up)] = "Up Arrow";
input_key_name[? string(vk_down)] = "Down Arrow";
input_key_name[? string(vk_enter)] = "Enter";
input_key_name[? string(vk_escape)] = "Escape";
input_key_name[? string(vk_space)] = "Space";
input_key_name[? string(vk_backspace)] = "Backspace";
input_key_name[? string(vk_tab)] = "Tab";
input_key_name[? string(vk_home)] = "Home";
input_key_name[? string(vk_end)] = "End";
input_key_name[? string(vk_delete)] = "Delete";
input_key_name[? string(vk_insert)] = "Insert";
input_key_name[? string(vk_pageup)] = "Page Up";
input_key_name[? string(vk_pagedown)] = "Page Down";
input_key_name[? string(vk_pause)] = "Pause/Break";
input_key_name[? string(vk_printscreen)] = "Printscreen/SysRq";
input_key_name[? string(vk_f1)] = "F1";
input_key_name[? string(vk_f2)] = "F2";
input_key_name[? string(vk_f3)] = "F3";
input_key_name[? string(vk_f4)] = "F4";
input_key_name[? string(vk_f5)] = "F5";
input_key_name[? string(vk_f6)] = "F6";
input_key_name[? string(vk_f7)] = "F7";
input_key_name[? string(vk_f8)] = "F8";
input_key_name[? string(vk_f9)] = "F9";
input_key_name[? string(vk_f10)] = "F10";
input_key_name[? string(vk_f11)] = "F11";
input_key_name[? string(vk_f12)] = "F12";
input_key_name[? string(vk_numpad0)] = "Numpad 0";
input_key_name[? string(vk_numpad1)] = "Numpad 1";
input_key_name[? string(vk_numpad2)] = "Numpad 2";
input_key_name[? string(vk_numpad3)] = "Numpad 3";
input_key_name[? string(vk_numpad4)] = "Numpad 4";
input_key_name[? string(vk_numpad5)] = "Numpad 5";
input_key_name[? string(vk_numpad6)] = "Numpad 6";
input_key_name[? string(vk_numpad7)] = "Numpad 7";
input_key_name[? string(vk_numpad8)] = "Numpad 8";
input_key_name[? string(vk_numpad9)] = "Numpad 9";
input_key_name[? string(vk_multiply)] = "Numpad Multiply";
input_key_name[? string(vk_divide)] = "Numpad Divide";
input_key_name[? string(vk_add)] = "Numpad Add";
input_key_name[? string(vk_subtract)] = "Numpad Subtract";
input_key_name[? string(vk_decimal)] = "Numpad Decimal";

input_key_name[? string(vk_lshift)] = "Left Shift";
input_key_name[? string(vk_rshift)] = "Right Shift";
input_key_name[? string(vk_lalt)] = "Left Alt";
input_key_name[? string(vk_ralt)] = "Right Alt";
input_key_name[? string(vk_lcontrol)] = "Left Control";
input_key_name[? string(vk_rcontrol)] = "Right Control";
input_key_name[? string(vk_lshift)] = "Left Shift";

input_key_name[? string(ord("Q"))] = "Q";
input_key_name[? string(ord("W"))] = "W";
input_key_name[? string(ord("E"))] = "E";
input_key_name[? string(ord("R"))] = "R";
input_key_name[? string(ord("T"))] = "T";
input_key_name[? string(ord("Y"))] = "Y";
input_key_name[? string(ord("U"))] = "U";
input_key_name[? string(ord("I"))] = "I";
input_key_name[? string(ord("O"))] = "O";
input_key_name[? string(ord("P"))] = "P";
input_key_name[? string(ord("A"))] = "A";
input_key_name[? string(ord("S"))] = "S";
input_key_name[? string(ord("D"))] = "D";
input_key_name[? string(ord("F"))] = "F";
input_key_name[? string(ord("G"))] = "G";
input_key_name[? string(ord("H"))] = "H";
input_key_name[? string(ord("J"))] = "J";
input_key_name[? string(ord("K"))] = "K";
input_key_name[? string(ord("L"))] = "L";
input_key_name[? string(ord("Z"))] = "Z";
input_key_name[? string(ord("X"))] = "X";
input_key_name[? string(ord("C"))] = "C";
input_key_name[? string(ord("V"))] = "V";
input_key_name[? string(ord("B"))] = "B";
input_key_name[? string(ord("N"))] = "N";
input_key_name[? string(ord("M"))] = "M";

input_key_name[? string(ord("1"))] = "1";
input_key_name[? string(ord("2"))] = "2";
input_key_name[? string(ord("3"))] = "3";
input_key_name[? string(ord("4"))] = "4";
input_key_name[? string(ord("5"))] = "5";
input_key_name[? string(ord("6"))] = "6";
input_key_name[? string(ord("7"))] = "7";
input_key_name[? string(ord("8"))] = "8";
input_key_name[? string(ord("9"))] = "9";
input_key_name[? string(ord("0"))] = "0";

// Misc Keys, may not be completely compatible
input_key_name[? "192"] = "~";
input_key_name[? "189"] = "-";
input_key_name[? "187"] = "+";
input_key_name[? "219"] = "[";
input_key_name[? "221"] = "]";
input_key_name[? "186"] = ";";
input_key_name[? "222"] = "'";
input_key_name[? "220"] = "\\";
input_key_name[? "188"] = ",";
input_key_name[? "190"] = ".";
input_key_name[? "191"] = "?";
input_key_name[? "20"] = "Caps Lock";
input_key_name[? "144"] = "Num Lock";
input_key_name[? "93"] = "Context Menu";
input_key_name[? "91"] = "Windows Key";

// Gamepad Names
var t_id = input_gp_type.button;

input_gp_name[? string(t_id) + ":" + string(gp_face1)] = "Button 1";
input_gp_name[? string(t_id) + ":" + string(gp_face2)] = "Button 2";
input_gp_name[? string(t_id) + ":" + string(gp_face3)] = "Button 3";
input_gp_name[? string(t_id) + ":" + string(gp_face4)] = "Button 4";
input_gp_name[? string(t_id) + ":" + string(gp_padd)] = "D-Pad Down";
input_gp_name[? string(t_id) + ":" + string(gp_padu)] = "D-Pad Up";
input_gp_name[? string(t_id) + ":" + string(gp_padr)] = "D-Pad Right";
input_gp_name[? string(t_id) + ":" + string(gp_padl)] = "D-Pad Left";
input_gp_name[? string(t_id) + ":" + string(gp_select)] = "Select";
input_gp_name[? string(t_id) + ":" + string(gp_shoulderl)] = "Left Shoulder";
input_gp_name[? string(t_id) + ":" + string(gp_shoulderr)] = "Right Shoulder";
input_gp_name[? string(t_id) + ":" + string(gp_shoulderlb)] = "Left Trigger";
input_gp_name[? string(t_id) + ":" + string(gp_shoulderrb)] = "Right Trigger";
input_gp_name[? string(t_id) + ":" + string(gp_stickl)] = "Left Analog Button";
input_gp_name[? string(t_id) + ":" + string(gp_stickr)] = "Right Analog Button";
input_gp_name[? string(t_id) + ":" + string(gp_start)] = "Start";

t_id = input_gp_type.axis_lh;
input_gp_name[? string(t_id) + ":" + string(1)] = "Left Analog Right";
input_gp_name[? string(t_id) + ":" + string(-1)] = "Left Analog Left";

t_id = input_gp_type.axis_lv;
input_gp_name[? string(t_id) + ":" + string(1)] = "Left Analog Down";
input_gp_name[? string(t_id) + ":" + string(-1)] = "Left Analog Up";

t_id = input_gp_type.axis_rh;
input_gp_name[? string(t_id) + ":" + string(1)] = "Right Analog Right";
input_gp_name[? string(t_id) + ":" + string(-1)] = "Right Analog Left";

t_id = input_gp_type.axis_rv;
input_gp_name[? string(t_id) + ":" + string(1)] = "Right Analog Down";
input_gp_name[? string(t_id) + ":" + string(-1)] = "Right Analog Up";
