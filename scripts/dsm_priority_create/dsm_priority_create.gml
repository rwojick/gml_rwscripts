/// @desc DSM - Create Priority [priority_id]
/// @arg *group_name

// Inputs
var name = argument_count >= 1 ? argument[0] : global.dsm_default;

// Function Vars
var priority = ds_priority_create(),
	group = global.dsm_group[? name],
	ds = undefined;

if (is_undefined(group))
{
	if (dsm_debug)
	{
		show_error("DSM - Create Priority, Group does not exist: " + name, false);
		
		return (undefined);
	}
}

ds[0] = dsm_type.priority;
ds[1] = priority;

ds_list_add(group, ds);

return (priority);
