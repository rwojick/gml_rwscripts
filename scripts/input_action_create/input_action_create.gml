/// @desc Input System - Create Action [success]
/// @arg action_name
/// @arg trigger_key
/// @arg *trigger_gp_type
/// @arg *trigger_gp_value
/// @arg *active

var name = argument[0],
	key = argument[1],
	gp_type = argument_count >= 3 ? argument[2] : input_gp_type.disabled,
	gp_val = argument_count >= 4 ? argument[3] : undefined,
    active = argument_count >= 5 ? argument[4] : true;

if (is_undefined(input_action[? name]))
{
	var action = ds_map_create();
	
    input_value[? name] = -1;
	
	action[? "trig_key"] = key;
	action[? "trig_gp_type"] = gp_type;
	action[? "trig_gp_val"] = gp_val;
	
	input_action[? name] = action;
	
	if (active)
	{
		if (ds_list_find_index(input_active, name) == -1)
		{
			ds_list_add(input_active, name);
		}
	}

    return (true);
}

return (false);
