/// @desc Log
/// @arg text
/// @arg *clear
/// @arg *file_name

#macro log_file "log.txt"

var txt = string(argument[0]),
	clear = argument_count >= 2 ? argument[1] : false,
	filename = argument_count >= 3 ? string(argument[2]) : log_file;

var file = clear ? file_text_open_write(filename) : file_text_open_append(filename);

file_text_write_string(file, txt);
file_text_writeln(file);

file_text_close(file);
