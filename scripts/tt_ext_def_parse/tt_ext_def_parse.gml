/// @func tt_ext_def_parse(data) = tag_data;
/// @desc Tag Text - Default Ext. Parse Event
/// @arg data
/// @return tag_data

#region Declarations
	var data = argument[0];
	
	var tag = undefined,
		type = global.tt_tags[? data[0]];

#endregion

#region Main
	switch (data[0])
	{
		case tt_word:
			tag[tt_node.type] = tt_word;
			tag[tt_node.data] = data[1];
			tag[tt_node.w1] = string_width(data[1]);
			tag[tt_node.h1] = string_height(data[1]);
			tag[tt_node.len] = string_length(data[1]);
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			break;
		case tt_space:
			tag[tt_node.type] = tt_space;
			tag[tt_node.data] = " ";
			tag[tt_node.w1] = string_width(" ");
			tag[tt_node.h1] = string_height(" ");
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = true;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			break;
		case tt_newline:
			tag[tt_node.type] = tt_newline;
			tag[tt_node.data] = undefined;
			tag[tt_node.w1] = string_width("\n");
			tag[tt_node.h1] = string_height("\n");
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = true;
			tag[tt_node.draw] = type[? "draw"];
			break;
		case tt_tag_open:
			tag[tt_node.type] = tt_tag_open;
			tag[tt_node.data] = tt_open;
			tag[tt_node.w1] = string_width(tt_open);
			tag[tt_node.h1] = string_height(tt_open);
			tag[tt_node.len] = 1;
			tag[tt_node.is_wb] = false;
			tag[tt_node.is_nl] = false;
			tag[tt_node.draw] = type[? "draw"];
			break;
		default:
			// Shouldn't ever happen, but just in case...
			tag = undefined;
			break;
	}
	
	return (tag);
#endregion
