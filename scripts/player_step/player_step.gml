/// @desc Player - Step

var mx = input_check("right") - input_check("left"),
	my = input_check("down") - input_check("up");

hsp = mx * (mv_sp + (4 * input_check("cancel")));
vsp = my *  (mv_sp + (4 * input_check("cancel")));

x += hsp;
y += vsp;

if (input_check_pressed("accept"))
{
	//obj_master.toggle_tt = !obj_master.toggle_tt;
	//sy_test();
	array_test();
}

if (input_check_pressed("menu"))
{
	game_end();
}

if (input_check_pressed("cancel"))
{
	instance_destroy(obj_master);
	instance_create_depth(0, 0, 0, obj_master);
}
