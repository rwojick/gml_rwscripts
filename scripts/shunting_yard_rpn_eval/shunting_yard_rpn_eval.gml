/// @description  shunting_yard_rpn_eval(rpn_grid) = result;
/// @function  shunting_yard_rpn_eval
/// @param rpn_grid

// NOTE:  PARAMETERS ARE READ IN REVERSE ORDER, FOR BOTH FUNCTIONS AND OPERATORS!

var rpn_grid = argument[0];

if (ds_exists(rpn_grid, ds_type_grid))
{
    var stack = ds_stack_create(),
        c1 = 0;
    
    while (c1 < ds_grid_height(rpn_grid))
    {
        switch (rpn_grid[# 1, c1])
        {
            case token_type.num:
            case token_type.str:
                ds_stack_push(stack, rpn_grid[# 0, c1]);
                break;
            case token_type.variable:
                ds_stack_push(stack, rpn_grid[# 0, c1]);
                break;
            case token_type.func:
            case token_type.op:
                var res = undefined,
                    error = "";
                
                switch (rpn_grid[# 0, c1])
                {
                    case "+":
                        if (ds_stack_size(stack) < 2)
                        {
                            error = "Syntax error:  Not enough operands for operator '+'.";
                        }
                        else
                        {
                            var v1 = ds_stack_pop(stack),
                                v2 = ds_stack_pop(stack);
                            
                            if (string_char_at(v1, 1) == "\"") or (string_char_at(v2, 1) == "\"")
                            {
                                v1 = string_replace_all(v1, "\"", "");
                                v2 = string_replace_all(v2, "\"", "");
                                
                                res = "\"" + v2 + v1 + "\"";
                            }
                            else
                            {
                                res = real(v2) + real(v1);
                            }
                        }
                        break;
                    case "-":
                        if (ds_stack_size(stack) < 2)
                        {
                            error = "Syntax error:  Not enough operands for operator '-'.";
                        }
                        else
                        {
                            var v1 = ds_stack_pop(stack),
                                v2 = ds_stack_pop(stack);
                            
                            if (string_char_at(v1, 1) == "\"") or (string_char_at(v2, 1) == "\"")
                            {
                                error = "Invalid data type:  Cannot subtract strings.";
                            }
                            else
                            {
                                res = real(v2) - real(v1);
                            }
                        }
                        break;
                    case "*":
                        if (ds_stack_size(stack) < 2)
                        {
                            error = "Syntax error:  Not enough operands for operator '*'.";
                        }
                        else
                        {
                            var v1 = ds_stack_pop(stack),
                                v2 = ds_stack_pop(stack);
                            
                            if (string_char_at(v1, 1) == "\"") or (string_char_at(v2, 1) == "\"")
                            {
                                error = "Invalid data type:  Cannot multiply strings.";
                            }
                            else
                            {
                                res = real(v2) * real(v1);
                            }
                        }
                        break;
                    case "/":
                        if (ds_stack_size(stack) < 2)
                        {
                            error = "Syntax error:  Not enough operands for operator '/'.";
                        }
                        else
                        {
                            var v1 = ds_stack_pop(stack),
                                v2 = ds_stack_pop(stack);
                            
                            if (string_char_at(v1, 1) == "\"") or (string_char_at(v2, 1) == "\"")
                            {
                                error = "Invalid data type:  Cannot divide strings.";
                            }
                            else
                            {
                                res = real(v2) / real(v1);
                            }
                        }
                        break;
                    case "\\":
                        if (ds_stack_size(stack) < 2)
                        {
                            error = "Syntax error:  Not enough operands for operator '\\'.";
                        }
                        else
                        {
                            var v1 = ds_stack_pop(stack),
                                v2 = ds_stack_pop(stack);
                            
                            if (string_char_at(v1, 1) == "\"") or (string_char_at(v2, 1) == "\"")
                            {
                                error = "Invalid data type:  Cannot integer divide strings.";
                            }
                            else
                            {
                                res = real(v2) div real(v1);
                            }
                        }
                        break;
                    case "%":
                        if (ds_stack_size(stack) < 2)
                        {
                            error = "Syntax error:  Not enough operands for operator '%'.";
                        }
                        else
                        {
                            var v1 = ds_stack_pop(stack),
                                v2 = ds_stack_pop(stack);
                            
                            if (string_char_at(v1, 1) == "\"") or (string_char_at(v2, 1) == "\"")
                            {
                                error = "Invalid data type:  Cannot mod strings.";
                            }
                            else
                            {
                                res = real(v2) % real(v1);
                            }
                        }
                        break;
                    case "-:":
                        if (ds_stack_size(stack) < 1)
                        {
                            error = "Syntax error:  Not enough operands for operator '-:'.";
                        }
                        else
                        {
                            var v1 = ds_stack_pop(stack);
                            
                            if (string_char_at(v1, 1) == "\"")
                            {
                                error = "Invalid data type:  Cannot negate strings.";
                            }
                            else
                            {
                                res = -(real(v1));
                            }
                        }
                        break;
                    case "null_func":
                        if (ds_stack_size(stack) < 1)
                        {
                            error = "Syntax error:  Not enough operands for function 'null_func'.";
                        }
                        else
                        {
                            var v1 = ds_stack_pop(stack);
                            
                            res = v1;
                        }
                        break;
                    case "max":
                        if (ds_stack_size(stack) < 2)
                        {
                            error = "Syntax error:  Not enough operands for function 'max'.";
                        }
                        else
                        {
                            var v1 = ds_stack_pop(stack),
								v2 = ds_stack_pop(stack);
                            
                            res = max(real(v2), real(v1));
                        }
                        break;
                    default:
                        error = "Syntax Error:  Unknown operator or function:" + rpn_grid[# 0, c1];
                        break;
                }
                
                if (is_undefined(res))
                {
                    error = "Unknown Syntax Error!";
                }
                
                if (error)
                {
                    show_error(error, false);
                    
                    ds_stack_destroy(stack);
                    
                    return (undefined);
                }
                else
                {
                    ds_stack_push(stack, res);
                }
                break;
            default:
                show_error("Unknown error.", false);
                
                ds_stack_destroy(stack);
                
                return (undefined);
                break;
        }
        
        c1++;
    }
    
    if (ds_stack_size(stack) == 1)
    {
        var res = real(ds_stack_pop(stack));
        
        ds_stack_destroy(stack);
        
        return (res);
    }
    
    // Too many values.  Syntax error.
    show_error("Unknown error:  Too many values.", false);
    ds_stack_destroy(stack);
}

return (undefined);


