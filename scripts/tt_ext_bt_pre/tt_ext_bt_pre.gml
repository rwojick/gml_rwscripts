var str = argument[0];

// Convert "icon" literal to "icon char" tag
str = string_replace_all(str, tt_escape + tt_icon, tt_open + tt_icon_char + tt_close);		
		
// Convert "icon" shortcut to "icon" tags
var p1 = string_pos(tt_icon, str);
		
while (p1 != 0)
{
	var char = string_char_at(str, p1 + 1);
	str = string_replace_all(str, tt_icon + char, tt_open + tt_tag_icon + "|" + char + tt_close);
			
	p1 = string_pos(tt_icon, str);
}

return (str);
