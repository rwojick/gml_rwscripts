/// @func tt_parse(string) = tt_data;
/// @desc Tag Text - Parse
/// @arg string
/// @return tagtext_data

#region Declarations
	var orig_str = string(argument[0]);
	
	var str = orig_str,
		c1 = 0,
		p1 = 0,
		p2 = 0,
		data = [], // Holds the tag nodes
		w1 = 0,
		h1 = 0,
		len = 0,
		cw = 0,
		ch = 0,
		tt = []; // Overall parsed tag text data, including size
#endregion

#region Main
	if (str = "")
	{
		return ([]);
	}
	
	#region Pre-Parse
		var pre = ds_map_find_first(global.tt_exts);
		
		repeat (ds_map_size(global.tt_exts))
		{
			var ext = global.tt_exts[? pre];
			
			if (script_exists(ext[? "pre"]))
			{
				str = script_execute(ext[? "pre"], str);
			}
			
			pre = ds_map_find_next(global.tt_exts, pre);
		}
	#endregion
	
	tt_set_font(tt_font_def);
	
	while (str != "")
	{
		var tag = undefined;
		
		p1 = string_pos(tt_open, str);
		
		if (p1 == 0)
		{
			if (str != "")
			{
				tag = tt_tag_parse([tt_word, str]);
			}
			str = "";
			
			if (!is_undefined(tag))
			{
				data[c1] = tag;
				c1++;
				
				// Calc overall size
				cw = max(cw, tag[tt_node.h1]);
				
				if (tag[tt_node.is_nl])
				{
					if (cw > w1)
					{
						w1 = cw;
					}
					
					cw = 0;
					h1 += ch;
					ch = 0;
				}
				else
				{
					cw += tag[tt_node.w1];
				}
				
				len += tag[tt_node.len];
				// end calc
			}
		}
		else
		{
			var word = string_copy(str, 1, p1 - 1);
			
			if (word != "")
			{
				tag = tt_tag_parse([tt_word, word]);
			}
			
			str = string_delete(str, 1, p1);
			
			if (!is_undefined(tag))
			{
				data[c1] = tag;
				c1++;
				
				// Calc overall size
				cw = max(cw, tag[tt_node.h1]);
				
				if (tag[tt_node.is_nl])
				{
					if (cw > w1)
					{
						w1 = cw;
					}
					
					cw = 0;
					h1 += ch;
					ch = 0;
				}
				else
				{
					cw += tag[tt_node.w1];
				}
				
				len += tag[tt_node.len];
				// end calc
			}
			
			p2 = string_pos(tt_close, str);
			
			if (p2 == 0)
			{
				show_error("Invalid Tag Text string: " + orig_str, false);
				return ([]);
			}
			else
			{
				var code = string_copy(str, 1, p2 - 1);
				
				str = string_delete(str, 1, p2);
				
				tag = tt_tag_parse(string_split(code));
				
				if (!is_undefined(tag))
				{
					data[c1] = tag;
					c1++;

					// Calc overall size
					cw = max(cw, tag[tt_node.h1]);
				
					if (tag[tt_node.is_nl])
					{
						if (cw > w1)
						{
							w1 = cw;
						}
					
						cw = 0;
						h1 += ch;
						ch = 0;
					}
					else
					{
						cw += tag[tt_node.w1];
					}
				
					len += tag[tt_node.len];
					// end calc
				}
			}
		}
	}
	
	tt[tt_index.nodes] = data;
	tt[tt_index.w1] = w1;
	tt[tt_index.h1] = h1;
	tt[tt_index.len] = len;
	
	return (tt);
#endregion
