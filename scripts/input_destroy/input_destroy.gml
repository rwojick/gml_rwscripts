/// @desc Input Control - Destroy

var key = "";

ds_map_destroy(input_group); // Values stored as array, does not need iterating
ds_map_destroy(input_triggered);
ds_map_destroy(input_virts);
ds_map_destroy(input_value);
ds_map_destroy(input_key_name);
ds_map_destroy(input_gp_name);
ds_list_destroy(input_active);

key = ds_map_find_first(input_action);

repeat (ds_map_size(input_action))
{
	var action = input_action[? key];
	
	ds_map_destroy(action);
	
	key = ds_map_find_next(input_action, key);
}

ds_map_destroy(input_action);
