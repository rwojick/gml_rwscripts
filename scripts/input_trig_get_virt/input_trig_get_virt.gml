/// @desc Input System - Get Virtual Input Triggered
/// @arg action_name
/// @return trigger_state

var name = argument[0];

return (!is_undefined(input_virts[? name]) ? true : false);
