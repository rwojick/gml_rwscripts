/// @desc Input System - Get Action Trigger Key
/// @arg action_name
/// @return key_id or undefined

var name = string(argument[0]);

if (!is_undefined(input_action[? name]))
{
	var action = input_action[? name];
	
	return (action[? "trig_key"]);
}

return (undefined);
