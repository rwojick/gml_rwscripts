/// @desc DS Stack to Debug String
/// @arg ds_stack

// Input vars
var st = argument[0];

// Script vars
var str = "{ ";

if (!ds_exists(st, ds_type_stack))
{
    return ("");
}
else
{
    var st0 = ds_stack_create();
    
    ds_stack_copy(st0, st);
    
    repeat (ds_stack_size(st0))
    {
        str += string(ds_stack_pop(st0)) + ", ";
    }
    
    str += " }";
    
    ds_stack_destroy(st0);
}

return (str);
