var atk = 50,
	def = 0,
	dmg = 0,
	ppct = 0.5;
	
var r1 = 10;

var pdmg = atk * ppct;

log("Base DMG:", true, "dmg.txt");

repeat (r1)
{
	dmg = max(atk - def, 0);
	log(dmg, false, "dmg.txt");
	def += 10;
}

log("Def % Test:", false, "dmg.txt");

def = 0;

log("Min DMG Test:", false, "dmg.txt");

repeat (r1)
{
	dmg = max(pdmg, atk - def, 0);
	log(dmg, false, "dmg.txt");
	def += 10;
}

log("Def % Test:", false, "dmg.txt");

def = 0;

repeat (r1)
{
	dmg = max(0, atk - (def * ppct));
	log(dmg, false, "dmg.txt");
	def += 10;	
}

msg("done");
