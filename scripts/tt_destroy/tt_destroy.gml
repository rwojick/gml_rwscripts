/// @func tt_destroy() = success;
/// @desc Tag Text - Destroy
/// @return success

#region Declarations
	var key = "";
#endregion

#region Single Line Clean Up
#endregion

#region Multi-Line Clean Up
	// Extentions need to be destroyed first to perform proper clean up
	key = ds_map_find_first(global.tt_exts);
	
	repeat (ds_map_size(global.tt_exts))
	{
		var ext = global.tt_exts[? key];
		
		if (script_exists(ext[? "destroy"]))
		{
			script_execute(ext[? "destroy"]);
		}
		
		ds_map_destroy(ext);
		
		key = ds_map_find_next(global.tt_exts, key);
	}
	
	ds_map_destroy(global.tt_exts);
	
	// Tags
	key = ds_map_find_first(global.tt_tags);
	
	repeat (ds_map_size(global.tt_tags))
	{
		var tag = global.tt_tags[? key];
		
		ds_map_destroy(tag);
		
		key = ds_map_find_next(global.tt_tags, key);
	}
	
	ds_map_destroy(global.tt_tags);
	
	// Res Types
	key = ds_map_find_first(global.tt_res_types);
	
	repeat (ds_map_size(global.tt_res_types))
	{
		var type = global.tt_res_types[? key];
		
		ds_map_destroy(type);
		
		key = ds_map_find_next(global.tt_res_types, key);
	}
	
	ds_map_destroy(global.tt_res_types);
#endregion
