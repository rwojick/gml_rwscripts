/// @desc String Format Simple
/// @arg string
/// @arg *value
/// @arg *...

// Inputs
var str = argument[0];

// Script Vars
var c1 = argument_count - 1;

repeat (argument_count - 1)
{
    str = string_replace_all(str, "$" + string(c1 - 1), string(argument[c1]));
    
    c1--;
}

return (str);
