/// @desc Input Control - Initialize

// Macros
#macro input_gp_disabled -1
#macro input_config_file "input_system.config"
#macro gp_facedn gp_face1
#macro gp_facert gp_face2
#macro gp_facelf gp_face3
#macro gp_faceup gp_face4
#macro gp_facea gp_face1
#macro gp_faceb gp_face2
#macro gp_facex gp_face3
#macro gp_facey gp_face4

// Enums
enum input_type
{
	none = -1,
	key,
	gp,
	virt
}

enum input_gp_type
{
    disabled = -1,
    button,
    axis_lh,
    axis_lv,
    axis_rh,
    axis_rv
}

// Globals
global.input_id = id;

// System values
input_paused = false; // Locally pauses the Input system.  does not disable the system, so no values are reset.

input_action = ds_map_create(); // Contains all data related to actions, except thier current values
input_value = ds_map_create(); // Current values of the actions, integer data
input_group = ds_map_create(); // Contains arrays of action IDs
input_active = ds_list_create(); // A list of active inputs, iterated through on step

input_time_clamp = room_speed * 60;
input_rep_delay = 15;
input_rep_interval = 3;

input_triggered = ds_map_create(); // Holds input states for each step, cleared at the end
input_virts = ds_map_create(); // Holds virtual trigger states for each step, cleared at the end, in map = pressed

input_key_name = ds_map_create();
input_gp_name = ds_map_create();

input_init_names();

input_gp_supported = gamepad_is_supported();
input_gp_enabled = true;
input_gp_id = input_gp_disabled; // Stores the current Device ID for Gamepad use
input_gp_axis_threshold = 0.5;
input_gp_axis_deadzone = 0.1;
input_gp_button_threshold = 0.1;

input_gp_connected = []; // Array containing connected status of GP Ids
input_gp_num_devices = gamepad_get_device_count();

if (input_gp_supported)
{
	//input_gp_get_connected();
	input_gp_connected = array_create(input_gp_num_devices);
	
	input_gp_get_device();
}
