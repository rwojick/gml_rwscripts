/// @desc Input System - Save Data

var file = file_text_open_write(input_config_file),
	data = buffer_create(256, buffer_grow, 1),
	key = ds_map_find_first(input_action),
	str = "";

buffer_write(data, buffer_bool, input_gp_enabled);
buffer_write(data, buffer_u16, ds_map_size(input_action));

repeat (ds_map_size(input_action))
{
	buffer_write(data, buffer_string, key);
	buffer_write(data, buffer_string, ds_map_write(input_action[? key]))
	
	key = ds_map_find_next(input_action, key);
}

str = buffer_base64_encode(data, 0, buffer_get_size(data));

file_text_write_string(file, str);

file_text_close(file);

buffer_delete(data);
