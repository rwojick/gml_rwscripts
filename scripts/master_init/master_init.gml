/// @desc Master - Init

#region Macros and Enums

#macro msg show_message 
#macro rnd irandom_range 
#macro rgb make_color_rgb

#macro debug true

#endregion

#region Input Control

input_init();
input_test();

#endregion

randomize();

get_key = false;
last_key = 0;

draw_set_font(font_main);

tt_init();

tt_add_res(tt_rt_font, tt_font_def, font_main);
tt_add_res(tt_rt_font, tt_font_icon, font_icon);
tt_add_res(tt_rt_font, "scary", font_scary);

tt_add_res(tt_rt_color, "red", c_red);

tt_add_res(tt_rt_var, "name", "Robert");

var icon = chr(rnd(32, 127));

tt = tt_parse("Hello, World!\nI am the next line!\n \\[fooled_you] \nTesting: \\$ \nSymbol Test: '$" + icon + "'\n[font|scary]This is [color|red]SPOOKY[d]!\n[rgb|0|128|0]Colorful[r]...End?\nNope, my name is [var|name]!\nEND");

tt_log(tt);

toggle_tt = false;
