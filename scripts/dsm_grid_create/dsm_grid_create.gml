/// @desc DSM - Create Grid [grid_id]
/// @arg width
/// @arg height
/// @arg *group_name

// Inputs
var width = argument[0],
	height = argument[1],
	name = argument_count >= 3 ? argument[2] : global.dsm_default;

// Function Vars
var grid = ds_grid_create(width, height),
	group = global.dsm_group[? name],
	ds = undefined;

if (is_undefined(group))
{
	if (dsm_debug)
	{
		show_error("DSM - Create Grid, Group does not exist: " + name, false);
		
		return (undefined);
	}
}

ds[0] = dsm_type.grid;
ds[1] = grid;

ds_list_add(group, ds);

return (grid);
