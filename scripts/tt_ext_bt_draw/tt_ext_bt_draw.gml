/// @func tt_ext_def_draw(x1, y1, data) = success;
/// @desc Tag Text - Default Ext. Draw Event
/// @arg x1
/// @arg y1
/// @arg data
/// @return success

#region Declarations
	var x1 = argument[0],
		y1 = argument[1],
		data = argument[2];
#endregion

switch (data[tt_node.type])
{
	case tt_tag_icon:
		draw_set_font(tt_get_res(tt_rt_font, tt_font_icon));
		draw_text(x1, y1, data[tt_node.data]);
		draw_set_font(global.tt_font);
		break;
	case tt_icon_char:
		draw_text(x1, y1, data[tt_node.data]);	
		break;
	case tt_color:
		draw_set_color(data[tt_node.data]);
		break;
	case tt_rgb:
		draw_set_color(data[tt_node.data]);
		break;
	case tt_tag_font:
		tt_set_font(data[tt_node.data]);
		break;
	case tt_var:
		draw_text(x1, y1, data[tt_node.data]);
		break;
	case tt_def:
		draw_set_color(data[tt_node.data]);
		break;
	case tt_reset:
		var reset = data[tt_node.data];
		draw_set_color(reset[0]);
		tt_set_font(reset[1]);
		break;
	default:
		// Do nothing
		break;
}

return (true);
