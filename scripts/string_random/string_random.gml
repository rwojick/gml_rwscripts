/// @desc String Random
/// @arg length
/// @arg *character_map

// Inputs
var len = argument[0],
	map = argument_count >= 2 ? string(argument[1]) : "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";

// Output
var str = "";

// Script Vars
var mlen = string_length(map);

if (len <= 0) or (mlen <= 0)
{
	return ("");
}

repeat (len)
{
	str += string_char_at(map, irandom_range(1, mlen));
}

return (str);
