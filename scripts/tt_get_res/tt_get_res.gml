/// @func tt_get_res(resource_type, resource_name) = resource_value;
/// @desc Tag Text - Get Resource
/// @arg resource_type
/// @arg resource_name
/// @return resource_value

var type = string(argument[0]),
	name = string(argument[1]);

var val = undefined,
	res = global.tt_res_types[? type];

if (is_undefined(res))
{
	return (undefined);
}

return (res[? name]);
