input_action_create("accept", ord("X"), input_gp_type.button, gp_facea);
input_action_create("cancel", ord("Z"), input_gp_type.button, gp_faceb);
input_action_create("menu", vk_escape, input_gp_type.button, gp_start);
input_action_create("right", vk_right, input_gp_type.button, gp_padr);
input_action_create("up", vk_up, input_gp_type.button, gp_padu);
input_action_create("left", vk_left, input_gp_type.button, gp_padl);
input_action_create("down", vk_down, input_gp_type.button, gp_padd);

input_group_create("main", "accept", "cancel", "right", "up", "left", "down", "menu");
