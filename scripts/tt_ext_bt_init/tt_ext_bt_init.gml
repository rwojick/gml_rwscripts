#macro tt_icon "$"
#macro tt_tag_icon "icon"
#macro tt_icon_char "icon_char"
#macro tt_font_icon "icon"
#macro tt_color "color"
#macro tt_rgb "rgb"
#macro tt_tag_font "font"
#macro tt_var "var" // Resource, auto cast to basic string, will not be parsed for tags, does not support newline characters
#macro tt_def "d" // Reset color to default
#macro tt_reset "r" // Reset all values to default (color, font)
#macro tt_rt_color "color"
#macro tt_rt_var "var"
#macro tt_col_def "default"

tt_add_res(tt_rt_font, tt_font_icon, noone);

tt_add_res_type(tt_rt_color);
tt_add_res(tt_rt_color, tt_col_def, c_white);

tt_add_res_type(tt_rt_var);

/* Tags
icon:		char
icon_char:	n/a
color:		color_id
rgb:		red, blue, green
font:		font_id
var:		var_id
d:			n/a
r:			n/a
*/

/* PreParse Features
$x:		Print Icon, where 'x' is the character in the icon font to draw
\$:		Icon Char
*/

