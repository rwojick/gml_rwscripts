/// @func tt_draw(x1, y1, tt_data) = success;
/// @desc Tag Text - Draw
/// @arg x1
/// @arg y1
/// @arg tt_data
/// @return success

var x0 = argument[0],
	y0 = argument[1],
	tt_data = argument[2];

var x1 = x0,
	y1 = y0,
	c1 = 0,
	line_h1 = 0,
	align_off = 0,
	nodes = [];

if (is_string(tt_data))
{
	tt_data = tt_parse(tt_data);
}

switch (global.tt_align)
{
	case tt_alignment.left:
		align_off = 0;
		break;
	case tt_alignment.center:
		align_off = tt_data[tt_index.w1] / 2; // width / 2
		break;
	case tt_alignment.right:
		align_off = tt_data[tt_index.w1]; // width
		break;
}

nodes = tt_data[tt_index.nodes];

repeat (array_length_1d(nodes))
{
	var node = nodes[c1];
	
	script_execute(node[tt_node.draw], x1 - align_off, y1, node);
	
	x1 += node[tt_node.w1];
	line_h1 = max(line_h1, node[tt_node.h1]);
	
	if (node[tt_node.is_nl])
	{
		y1 += line_h1;
		line_h1 = 0;
		x1 = x0;
	}
	
	c1++;
}

return (true);
