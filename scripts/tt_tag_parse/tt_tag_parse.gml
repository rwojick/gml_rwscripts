/// @func tt_tag_parse(data) = tag_data;
/// @desc Tag Text - Parse Tag
/// @arg data
/// @return tag_data;

var data = argument[0];

var tag_type = global.tt_tags[? data[0]],
	tag = undefined;

if (is_undefined(tag_type))
{
	return (undefined);
}

if (script_exists(tag_type[? "parse"]))
{
	tag = script_execute(tag_type[? "parse"], data);
}

return (tag);
