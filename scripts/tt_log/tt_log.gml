var tt_data = argument[0];

var c1 = 0,
	nodes = [];

log("Tag Text Test:", true, "tt_test.txt");

nodes = tt_data[tt_index.nodes];

repeat (array_length_1d(nodes))
{
	var t = nodes[c1];
	
	var str = strf("$0 $1 $2 $3 $4 $5 $6 $7", t[0], t[1], t[2], t[3], t[4], t[5], t[6], script_get_name(t[7]));
	
	log(str, false, "tt_test.txt");
	
	c1++;
}
