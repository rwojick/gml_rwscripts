/// @desc Error Message
/// @arg error_text
/// @return success

#macro error_file "errors.txt"

if (debug)
{
	var txt = string(argument[0]);

	log(txt, false, error_file);
	show_error(txt, false);
}
