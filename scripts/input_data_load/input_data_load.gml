/// @desc Input System - Load Data

var file = file_text_open_read(input_config_file),
	data = buffer_base64_decode(file_text_read_string(file));

file_text_close(file);

input_gp_enabled = buffer_read(data, buffer_bool);

repeat (buffer_read(data, buffer_u16))
{
	var key = buffer_read(data, buffer_string),
		map = buffer_read(data, buffer_string);
	
	ds_map_read(input_action[? key], map);
}

buffer_delete(data);
