/// @desc NDS Control - Add DS
/// @arg ds_id
/// @arg prev_data
/// @return success

// Note:  For internal system use only.

var ds = argument[0],
	data = argument[1];

switch (data[0])
{
	case nds_type.map:
		var map = data[1];
		
		map[? data[2]] = ds;
		break;
	case nds_type.list:
		var list = data[1];
		
		list[| data[2]] = ds;
		break;
	case nds_type.grid:
		var grid = data[1],
			index = data[2];
			
		grid[# index[1], index[0]] = ds;
		break;
	case nds_type.array1d:
		var arr = data[1];
		
		arr[@ data[2]] = ds;
		break;
	case nds_type.array2d:
		var arr = data[1],
			index = data[2];
		
		arr[@ index[0], index[1]] = ds;
		break;
	default:
		// Shouldn't happen
		return (false);
		break;
}

return (true);
