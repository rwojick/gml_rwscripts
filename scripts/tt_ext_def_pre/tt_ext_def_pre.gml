/// @func tt_ext_def_pre(string) = string;
/// @desc Tag Text - Default Ext. Pre Parse Event
/// @arg string
/// @return string

var str = string(argument[0]);

// Convert "open" character to "open" tag
str = string_replace_all(str, tt_escape + tt_open, tt_open + tt_tag_open + tt_close);
	
// Convert "new line" character to "new line" tag
str = string_replace_all(str, "\n", tt_open + tt_newline + tt_close);
	
// Convert "space" character to "space" tag
str = string_replace_all(str, " ", tt_open + tt_space + tt_close);

return (str);
