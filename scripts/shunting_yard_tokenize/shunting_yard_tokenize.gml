/// @description  shunting_yard_tokenize(string) = token_grid;
/// @function  shunting_yard_tokenize
/// @param string

var str = string(argument[0]);

var token_list = ds_list_create(),
    curr_token = "",
    last_type = -1,
    c1 = 1,
    type_data = 0,
    token_grid = 0;

type_data[0] = "("; // Left Paren
type_data[1] = ")"; // Right Paren
type_data[2] = ","; // Comma
type_data[3] = "+-=%*/\\<>!|&^~:"; // Operator
type_data[4] = "\"" + "'"; // String Demarker
type_data[5] = "1234567890_qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM."; // Function/Variable/Number
type_data[6] = ";"; // Line terminator
type_data[7] = " " + chr(9); // White space

while (c1 <= string_length(str))
{
    var data = string_char_at(str, c1),
        type = -1,
        c2 = 0;
    
    repeat (array_length_1d(type_data))
    {
        if (string_pos(data, type_data[c2]) != 0)
        {
            type = c2;
            break;
        }
        
        c2++;
    }
    
    switch (type)
    {
        case 0: // Left Paren
            //
            break;
        case 1: // Right Paren
            //
            break;
        case 2: // Comma
            //
            break;
        case 3: // Operator
            //
            break;
        case 4: // String Demarker
            var char = "",
                dm = data;
            
            c1++;
            
            char = string_char_at(str, c1);
            
            data = "\"";
            
            do
            {
                data += char;
                char = string_char_at(str, ++c1);
            } until (char == dm) or (c1 > string_length(str));
            
            data += "\"";
            break;
        case 5: // Function/Variable/Number
            //
            break;
        case 6: // Terminator
            // 0
            break;
        case 7: // Whitespace
            data = "";
            break;
        case -1:
        default:
            // Invalid character.  Ignore.
            data = "";
            break;
    }
    
    if (type != -1)
    {
        if (type != last_type) and (curr_token != "")
        {
            if (curr_token == "-") and ((type == 0) or (type == 5))
            {
                curr_token += ":";
            }
            
            ds_list_add(token_list, curr_token);
            curr_token = "";
        }
        
        curr_token += data;
        
        last_type = type;
    }
    
    c1++;
    
    if (c1 > string_length(str)) and (curr_token != "")
    {
        ds_list_add(token_list, curr_token);
    }
}

// Convert to grid

c1 = 0;

token_grid = ds_grid_create(2, ds_list_size(token_list));

repeat (ds_list_size(token_list))
{
    token_grid[# 0, c1] = token_list[| c1];
    c1++;
}

ds_list_destroy(token_list);

return (token_grid);


