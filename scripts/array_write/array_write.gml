/// @desc Array Write to String
/// @arg array

// Inputs
var array = argument[0];

if (!is_array(array))
{
	return ("");
}

// Output
var str = "";

// Script Vars
var temp = ds_list_create();

temp[| 0] = array;

str = ds_list_write(temp);

ds_list_destroy(temp);

return (str);
