/// @desc Input System - Check Action Repeated
/// @arg action_name
/// @return action_status

// Checks if the button is pressed or held, but with a repeat threashold and delay

if (instance_exists(global.input_id))
{
    with (global.input_id)
    {
        var name = argument[0];
        
        if (!is_undefined(input_action[? name]))
        {
            return ((input_value[? name] == 1) or (input_value[? name] mod input_rep_interval == 0) and (input_value[? name] > input_rep_delay));
        }
    }
}

return (false);
