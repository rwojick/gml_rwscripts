/// @desc DSM - Group Free [success]
/// @arg *group_name

// Inputs
var name = argument_count >= 1 ? argument[0] : global.dsm_default;

if (is_undefined(global.dsm_group[? name]))
{
	if (dsm_debug)
	{
		show_error("DSM - Group Free, Group Name does not exist: " + name, false);
	}
	
	return (false);
}
else
{
	var group = global.dsm_group[? name],
		c1 = 0;
	
	repeat (ds_list_size(group))
	{
		var item = group[| c1];
		
		switch (item[0])
		{
			case dsm_type.map:
				ds_map_destroy(item[1]);
				break;
			case dsm_type.list:
				ds_list_destroy(item[1]);
				break;
			case dsm_type.grid:
				ds_grid_destroy(item[1]);
				break;
			case dsm_type.queue:
				ds_queue_destroy(item[1]);
				break;
			case dsm_type.priority:
				ds_priority_destroy(item[1]);
				break;
			default:
				if (dsm_debug)
				{
					show_error("DSM - Group Free, Invalid DS Type: " + string(item[0]), false);
				}
		}
		
		c1++;
	}
	
	if (name != global.dsm_default)
	{
		ds_list_destroy(group);
	
		ds_map_delete(global.dsm_group, name);
	}
	
	return (true);
}
