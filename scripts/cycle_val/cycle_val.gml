/// @desc Cycle Value
/// @arg current_value
/// @arg value_change
/// @arg min_value
/// @arg max_value

// Inputs
var curr_val = argument[0],
	mod_val = argument[1],
	min_val = min(argument[2], argument[3]),
	max_val = max(argument[2], argument[3]);

// Output
var new_val = 0;

// Scrip Vars
var delta = 0;

if (mod_val == 0)
{
    return (curr_val);
}

new_val = curr_val - min_val;
delta = max_val - min_val;

if (delta == 0)
{
    return (curr_val);
}

mod_val = (abs(mod_val) mod delta) * sign(mod_val);
    
new_val += mod_val;

if (new_val < 0)
{
    new_val += delta;
}

if (new_val >= delta)
{
    new_val -= delta;
}

new_val += min_val;

return (new_val);
