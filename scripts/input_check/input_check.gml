/// @desc Input System - Check Action
/// @arg action_name
/// @return action_status

// Checks if the button is pressed or held (>= 1)

if (instance_exists(global.input_id))
{
    with (global.input_id)
    {
        var name = argument[0];
        
        if (!is_undefined(input_action[? name]))
        {
            return (input_value[? name] >= 1);
        }
    }
}

return (false);
