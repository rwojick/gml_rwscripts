/// @desc DS Grid Add Row
/// @arg ds_grid
/// @arg value
/// @arg *value

// Input Vars
var grid_id = argument[0];

if (ds_exists(grid_id, ds_type_grid))
{
    var w1 = ds_grid_width(grid_id),
        h1 = ds_grid_height(grid_id),
        c1 = 1;
    
    ds_grid_resize(grid_id, w1, ++h1);
    
    repeat (min(w1, argument_count - 1))
    {
        grid_id[# c1, h1] = argument[c1++];
    }
    
    return (true);
}

return (false);
