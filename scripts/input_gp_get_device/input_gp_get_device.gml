/// @desc Input System - Get New Gamepad Device ID
/// @return connected

/// Note:  Returns false if there are no connected gamepads, otherwise returns true

var c1 = 0;

repeat (input_gp_num_devices)
{
	if (input_gp_connected[c1])
	{
		input_gp_id = c1;
		
		return (true);
	}
	
	c1++;
}

input_gp_id = input_gp_disabled;

return (false);
