/// @desc Array Add
/// @arg array
/// @arg data
/// @arg *data

var array = argument[0];

var c1 = 0,
	len = array_length_1d(array);

repeat (argument_count - 1)
{
	array[@ len + c1] = argument[c1 + 1];
	
	c1++;
}
