/// @desc DS List to Debug String
/// @arg ds_list

// Inputs
var list = argument[0];

// Script Vars
var str = "{ ";

if (ds_exists(list, ds_type_list)) or (ds_list_size(list) > 0)
{
    var c1 = 0;

    repeat (ds_list_size(list))
    {
        str += string(list[| c1]) + ", ";
        
        c1++;
    }
}

str += " }";

return (str);
