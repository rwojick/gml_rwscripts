/// @desc DSM - Create List [list_id]
/// @arg *group_name

// Inputs
var name = argument_count >= 1 ? argument[0] : global.dsm_default;

// Function Vars
var list = ds_list_create(),
	group = global.dsm_group[? name],
	ds = undefined;
	
if (is_undefined(group))
{
	if (dsm_debug)
	{
		show_error("DSM - Create List, Group does not exist: " + name, false);
		
		return (undefined);
	}
}

ds[0] = dsm_type.list;
ds[1] = list;

ds_list_add(group, ds);

return (list);
