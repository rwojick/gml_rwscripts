/// @description  shunting_yard_rpn(token_grid) = rpn_grid;
/// @function  shunting_yard_rpn
/// @param token_grid

var token_grid = argument[0];

if (ds_exists(token_grid, ds_type_grid))
{
    var c1 = 0;
        
    // Create token grid [val, type]
    repeat (ds_grid_height(token_grid))
    {
        var token = token_grid[# 0, c1],
            type = -1;
        
        if (token == "(")
        {
            type = token_type.left_paren;
        }
        
        if (token == ")")
        {
            type = token_type.right_paren;
        }
        
        if (token == ",")
        {
            type = token_type.comma;
        }
        
        if (token == ";")
        {
            type = token_type.term;
        }
        
        if (type == token_type.none)
        {
            if (string_char_at(token, 1) == "\"")
            {
                type = token_type.str;
            }
            
            if (type == token_type.none)
            {
                // op, num, func
                if (string_pos(string_char_at(token, 1), "+-=%*/\\<>!|&^~:") != 0)
                {
                    type = token_type.op;
                }
                
                if (type == token_type.none)
                {
                    // Num, func
                    if (string_pos(string_char_at(token, 1), "0123456789.") != 0)
                    {
                        type = token_type.num;
                    }
                    else
                    {
                        // Add here:  Check next token for left paren == func, else var
                        type = token_type.func;
                    }
                }
            }
        }
        
        token_grid[# 1, c1++] = type;
    }
    
    // Type Grid created, begin Shunting Yard sorting
    var rpn_list = ds_list_create(), // Will be converted to grid at the end
        op_stack = ds_stack_create();
    
    c1 = 0;
    
    while (c1 < ds_grid_height(token_grid))
    {
        switch (token_grid[# 1, c1])
        {
            case token_type.num:
            case token_type.str:
                ds_list_add(rpn_list, c1);
                break;
            case token_type.func:
                ds_stack_push(op_stack, c1);
                break;
            case token_type.comma:
                var token = ds_stack_top(op_stack),
                    error = true;
                
                if (token_grid[# 1, token] == token_type.left_paren)
                {
                    error = false;
                }
                else
                {
                    while (!ds_stack_empty(op_stack)) and (token_grid[# 1, token] != token_type.left_paren)
                    {
                        token = ds_stack_top(op_stack);
                        
                        if (token_grid[# 1, token] == token_type.left_paren)
                        {
                            error = false;
                        }
                        else
                        {
                            ds_list_add(rpn_list, token);
                            
                            ds_stack_pop(op_stack);
                        }
                    }
                }
				
                if (error)
                {
                    show_error("RPN: Misplaced comma, or mismatched parenthesis.", false);
                    
                    ds_list_destroy(rpn_list);
                    ds_stack_destroy(op_stack);
                    
                    return (undefined);
                }
                break;
            case token_type.op:
                // Verify op token here
                var op_id = ds_grid_value_y(shyrd_ops, 0, 0, 0, ds_grid_height(shyrd_ops) - 1, token_grid[# 0, c1]);
                
                if (!ds_grid_value_exists(shyrd_ops, 0, 0, 0, ds_grid_height(shyrd_ops) - 1, token_grid[# 0, c1]))
                {
                    show_error("RPN: Invalid operator: " + token_grid[# 0, c1], false);
                    
                    ds_list_destroy(rpn_list);
                    ds_stack_destroy(op_stack);
                    
                    return (undefined);
                }
                else
                {
                    var t2 = -1,
                        op_id = ds_grid_value_y(shyrd_ops, 0, 0, 0, ds_grid_height(shyrd_ops) - 1, token_grid[# 0, c1]);
                    
                    while (t2 == -1) and (!ds_stack_empty(op_stack))
                    {
                        var op2_id = -1;
                        
                        t2 = ds_stack_top(op_stack);
                        
                        op2_id = ds_grid_value_y(shyrd_ops, 0, 0, 0, ds_grid_height(shyrd_ops) - 1, token_grid[# 0, t2]);
                        
                        if (token_grid[# 1, t2] == token_type.op) and (((shyrd_ops[# 2, op_id] == 0) and (shyrd_ops[# 1, op_id] >= shyrd_ops[# 1, op2_id])) or ((shyrd_ops[# 2, op_id] == 1) and (shyrd_ops[# 1, op_id] > shyrd_ops[# 1, op2_id])))
                        {
                            ds_list_add(rpn_list, t2);
                            
                            t2 = -1;
                            
                            ds_stack_pop(op_stack);
                        }
                        else
                        {
                            t2 = -2;
                        }
                    }
                    
                    ds_stack_push(op_stack, c1);
                }
                break;
            case token_type.left_paren:
                ds_stack_push(op_stack, c1);
                break;
            case token_type.right_paren:
                var t2 = ds_stack_pop(op_stack);
                
                while (token_grid[# 1, t2] != token_type.left_paren) and (!ds_stack_empty(op_stack))
                {
                    ds_list_add(rpn_list, t2);
                    t2 = ds_stack_pop(op_stack);
                }
                
                if (ds_stack_empty(op_stack)) and (token_grid[# 1, t2] != token_type.left_paren)
                {
                    show_error("RPN: Mismatched parenthesis.", false);
                    
                    ds_list_destroy(rpn_list);
                    ds_stack_destroy(op_stack);
                    
                    return (undefined);
                }
                
                t2 = ds_stack_top(op_stack);
                
                if (token_grid[# 1, t2] == token_type.func)
                {
                    ds_list_add(rpn_list, t2);
                    ds_stack_pop(op_stack);
                }
                break;
            default:
                // Do nothing
                break;
        }
        
        c1++;
    }
    
    while (!ds_stack_empty(op_stack))
    {
        var token = ds_stack_pop(op_stack);
        
        if (token_grid[# 1, token] == token_type.left_paren) or (token_grid[# 1, token] == token_type.right_paren)
        {
            show_error("RPN: Mismatched parenthesis." + string(token), false);
            
            ds_list_destroy(rpn_list);
            ds_stack_destroy(op_stack);
            
            return (undefined);
        }
        
        ds_list_add(rpn_list, token);
    }
    
    // Now, convert the list to a grid
    var rpn_grid = ds_grid_create(2, ds_list_size(rpn_list));
    
    c1 = 0;
    
    repeat (ds_list_size(rpn_list))
    {
        rpn_grid[# 0, c1] = token_grid[# 0, rpn_list[| c1]];
        rpn_grid[# 1, c1] = token_grid[# 1, rpn_list[| c1]];
        
        c1++;
    }
    
    ds_list_destroy(rpn_list);
    ds_stack_destroy(op_stack);
    
    return (rpn_grid);
}

show_error("RPN: Unspecified error.", false);

return (undefined);

