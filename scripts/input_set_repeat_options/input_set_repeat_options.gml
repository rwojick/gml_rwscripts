/// @desc Input System - Set Action Repeat Settings
/// @arg delay_value
/// @arg interval_value

var delay = argument[0],
    interval = argument[1];

input_rep_delay = delay;
input_rep_interval = interval;
