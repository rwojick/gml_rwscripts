/// @desc Input System - Get Keyboard Triggered
/// @arg action_name
/// @return trigger_state

var name = argument[0];

var action = input_action[? name];

return (keyboard_check(action[? "trig_key"]));
