/// @desc Input System - Detect Keycode Trigger
/// @return keycode

if (keyboard_key != 0)
{
    return (keyboard_key);
}

return (undefined);
