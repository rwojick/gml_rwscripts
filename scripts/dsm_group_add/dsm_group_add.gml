/// @desc DSM - Add Group [success]
/// @arg group_name

// Inputs
var name = argument[0];

if (!is_undefined(global.dsm_group[? name]))
{
	if (dsm_debug)
	{
		show_error("DSM - Add Group, Group already exists: " + name, false);
	}
	
	return (false);
}
else
{
	global.dsm_group[? name] = ds_list_create();
	
	return (true);
}
