/// @desc DSM - Create Queue [queue_id]
/// @arg *group_name

// Inputs
var name = argument_count >= 1 ? argument[0] : global.dsm_default;

// Function Vars
var queue = ds_queue_create(),
	group = global.dsm_group[? name],
	ds = undefined;

if (is_undefined(group))
{
	if (dsm_debug)
	{
		show_error("DSM - Create Queue, Group does not exist: " + name, false);
		
		return (undefined);
	}
}

ds[0] = dsm_type.queue;
ds[1] = queue;

ds_list_add(group, ds);

return (queue);
