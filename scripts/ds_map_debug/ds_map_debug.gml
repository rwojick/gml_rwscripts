/// @desc DS Map to Debug String
/// @arg ds_map

// Inputs
var map = argument[0];

// Script Vars
var str = "{ ";

if (ds_exists(map, ds_type_map)) or (ds_map_size(map) > 0)
{
    var key = ds_map_find_first(map);

    repeat (ds_map_size(map))
    {
        str += key + ":" + string(map[? key]) + ", ";
        
        key = ds_map_find_next(map, key);
    }
}

str += " }";

return (str);
