/// @desc Input System - Get Action Trigger Gamepad
/// @arg action_name
/// @return gamepad_id_array or undefined

var name = string(argument[0]);

if (!is_undefined(input_action[? name]))
{
	var action = input_action[? name],
		gp = undefined;
	
	gp[0] = action[? "trig_gp_type"];
	gp[1] = action[? "trig_gp_val"];	
	
	return (gp);
}

return (undefined);
