/// @desc Point to Index
/// @arg x
/// @arg y
/// @arg width
/// @return index

var x1 = argument[0],
	y1 = argument[1],
	w1 = argument[2];

return (x1 + (w1 * y1));
