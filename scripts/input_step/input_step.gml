/// @desc Input Control - Step Event

// Note:  Best used in the 'Begin Step' Event

if (!input_paused)
{
	var c1 = 0;
	
	// Verify current GP ID
	if (input_gp_id == input_gp_disabled) or (!input_gp_connected[input_gp_id])
	{
		input_gp_get_device();
	}
	
	repeat (ds_list_size(input_active))
	{
		var name = input_active[| c1];
		
		if (!is_undefined(input_action[? name]))
		{
			if (input_trig_get_key(name) or input_trig_get_gp(name) or input_trig_get_virt(name))
			{
				input_triggered[? name] = 0;
			}
		}
		
		c1++;
	}
	
	c1 = 0;
	
	repeat (ds_list_size(input_active))
	{
		var name = input_active[| c1];
		
		if (!is_undefined(input_triggered[? name]))
		{
			input_value[? name] = clamp(input_value[? name] + 1, 1, input_time_clamp)
		}
		else
		{
			input_value[? name] = clamp(input_value[? name] - 1, -(input_time_clamp), -1);
		}
		
		c1++;
	}
	
	ds_map_clear(input_triggered);
}

ds_map_clear(input_virts); // Virts needs to be cleared every step even if the system is paused so that presses don't pile up and trigger all in one step
