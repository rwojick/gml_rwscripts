/// @desc Input System - Get Key Trigger Name
/// @arg key_value
/// @return input_key_name

var key = string(argument[0]);

var name = input_key_name[? key];

name = is_undefined(name) ? "Keycode " + key : name;

return (name);
