/// @desc Input System - Set Action Trigger Key
/// @arg action_name
/// @arg key_id
/// @return success

// Inputs
var name = string(argument[0]),
	key = argument[1];

if (!is_undefined(input_action[? name]))
{
	var action = input_action[? name];
	
	action[? "trig_key"] = key;
	
	return (true);
}

return (false);
