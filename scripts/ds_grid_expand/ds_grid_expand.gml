/// @desc DS Grid Expand
/// @arg ds_grid
/// @arg width
/// @arg height

// Note:  Works like ds_grid_resize, but will only increase the size, not decrease.

// Input Vars
var grid = argument[0],
    w1 = argument[1],
    h1 = argument[2];

if (ds_exists(grid, ds_type_grid))
{
    w1 = max(w1, ds_grid_width(grid));
    h1 = max(h1, ds_grid_height(grid));
    
    ds_grid_resize(grid, w1, h1);
    
    return (true);
}

return (false);
