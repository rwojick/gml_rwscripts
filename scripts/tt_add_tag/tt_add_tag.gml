/// @func tt_add_tag(tag_name, script_parse, script_draw) = success;
/// @desc Tag Text - Add Tag
/// @arg tag_name
/// @arg script_parse
/// @arg script_draw
/// @return success

#region Declarations
	var name = string(argument[0]),
		scr_parse = argument[1],
		scr_draw = argument[2];
		
	var tag = global.tt_tags[? name];
#endregion

#region Error Checking + Validation
	// Disabled, this allows for tag overloading
	
	/*if (!is_undefined(global.tt_tags[? name]))
	{
		return (false);
	}*/
#endregion

#region Main
	if (is_undefined(tag))
	{
		tag = ds_map_create();
	}
	
	tag[? "parse"] = scr_parse;
	tag[? "draw"] = scr_draw;
	
	global.tt_tags[? name] = tag;
#endregion

return (true);
