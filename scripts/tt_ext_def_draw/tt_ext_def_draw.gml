/// @func tt_ext_def_draw(x1, y1, data) = success;
/// @desc Tag Text - Default Ext. Draw Event
/// @arg x1
/// @arg y1
/// @arg data
/// @return success

#region Declarations
	var x1 = argument[0],
		y1 = argument[1],
		data = argument[2];
#endregion

switch (data[tt_node.type])
{
	case tt_word:
		draw_text(x1, y1, data[tt_node.data]);
		break;
	case tt_space:
		draw_text(x1, y1, data[tt_node.data]);
		break;
	case tt_newline:
		// Do nothing
		break;
	case tt_tag_open:
		draw_text(x1, y1, data[tt_node.data]);		
		break;
	default:
		// Do nothing
		break;
}

return (true);
