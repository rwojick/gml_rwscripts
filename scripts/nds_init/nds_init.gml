/// @desc NDS Control - Init

// Macros and Enums
#macro nds_delim "."
#macro nds_map_key "?"
#macro nds_list_key "|"
#macro nds_grid_key "#"
#macro nds_array1d_key "@"
#macro nds_array2d_key "%"

enum nds_type
{
	none = -1,
	map,
	list,
	grid,
	array1d,
	array2d
}

// Globals
global.nds_id = id;
