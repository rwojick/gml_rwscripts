/// @desc Round Unstupid Version
/// @arg value

#macro round rround

// Inputs
var val = argument[0];

if (frac(val) >= 0.5)
{
    return (ceil(val));
}
else
{
    return (floor(val));
}
