/// @func tt_add_ext(ext_name, init_script, preparse_script, parse_script, draw_script, destroy_script, tag_name, *tag_name, *tag_name, *...) = success;
/// @desc Tag Text - Add Extention
/// @arg ext_name
/// @arg init_script
/// @arg preparse_script
/// @arg parse_script
/// @arg draw_script
/// @arg destroy_script
/// @arg tag_name
/// @arg *tag_name
/// @arg *...
/// @return success

var name = string(argument[0]),
	init = argument[1],
	pre = argument[2],
	parse = argument[3],
	draw = argument[4],
	dest = argument[5];

var c1 = 6,
	ext = undefined;

if (!is_undefined(global.tt_exts[? name]))
{
	return (false);	
}

ext = ds_map_create();

ext[? "init"] = init;
ext[? "pre"] = pre;
ext[? "parse"] = parse;
ext[? "draw"] = draw;
ext[? "destroy"] = dest;

script_execute(init);

repeat (argument_count - 6)
{
	tt_add_tag(argument[c1], parse, draw);
	c1++;
}

global.tt_exts[? name] = ext;

return (true);
