/// @desc NDS Control - Get String
/// @arg nds_id
/// @arg path
/// @arg *as_real
/// @return value

var nds = argument[0],
	path = string(argument[1]),
	as_real = argument_count >= 3 ? argument[2] : false;

var data = nds,
	index_list = string_split(path, nds_delim),
	len = array_length_1d(index_list),
	i = 0;

repeat (len)
{
	var index = index_list[i],
		type = string_char_at(index, 1);
	
	if (is_undefined(data))
	{
		return (undefined);
	}
	
	switch (type)
	{
		case nds_list_key:
			index = real(string_delete(index, 1, 1));
			
			if (ds_exists(data, ds_type_list))
			{
				data = data[| index];
			}
			else
			{
				return (undefined);
			}
			break;
		case nds_grid_key:
			index = string_delete(index, 1, 1);
			index = string_split_ext(index, ",");
			
			if (ds_exists(data, ds_type_grid))
			{
				data = data[# index[0], index[1]];
			}
			else
			{
				return (undefined);
			}
			break;
		case nds_array1d_key:
			index = real(string_delete(index, 1, 1));
			
			if (is_array(data))
			{
				data = data[index];
			}
			else
			{
				return (undefined);
			}
			break;
		case nds_array2d_key:
			index = string_delete(index, 1, 1);
			index = string_split_ext(index, ",");
			
			if (is_array(data))
			{
				data = data[index[0], index[1]];
			}
			else
			{
				return (undefined);
			}
			break;
		case nds_map_key:
			index = string_delete(index, 1, 1);
		default:
			if (ds_exists(data, ds_type_map))
			{
				data = data[? index];
			}
			else
			{
				return (undefined);
			}
			break;
	}
	
	i++;
}

if (is_undefined(data))
{
	return (undefined);
}

if (as_real)
{
	data = real(data);
}
else
{
	data = string(data);
}

return (data);
