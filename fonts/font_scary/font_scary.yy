{
    "id": "408340a6-6dfc-4879-986d-bf26e2ef80da",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_scary",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Chiller",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "095274d3-4960-4056-9161-95ac2587d82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 49,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 58,
                "y": 155
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d1346048-0b40-4f24-9191-1472aeb9c385",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 49,
                "offset": 3,
                "shift": 11,
                "w": 7,
                "x": 158,
                "y": 155
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ad9d7c9e-500f-4f04-8020-7e08f06c8403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 49,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 138,
                "y": 155
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2ded476b-bba7-4738-8006-811640ff0b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 49,
                "offset": 2,
                "shift": 46,
                "w": 43,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4f41c3f6-b7e3-4a8f-b7bc-8bff8ade75d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 49,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 188,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4a9c7a78-fcf9-4c6a-9d3b-29b04521e585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 49,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "83140859-cc3a-40fe-b410-23d16fe8dffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 49,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 466,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "28cd300e-2820-420c-870d-502b34e5d6fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 49,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 208,
                "y": 155
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "15ffb85f-fec9-423e-b1a7-176d4b87421c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 49,
                "offset": 4,
                "shift": 17,
                "w": 14,
                "x": 220,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "18d58f28-0f2f-4a64-b711-cc6050b69a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 49,
                "offset": 0,
                "shift": 17,
                "w": 14,
                "x": 348,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "36f35d7e-fe0e-42f9-9e9c-d917a61b0d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 49,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 139,
                "y": 104
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e2934567-c7cd-47f2-a147-75bc1881aa86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 49,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c7aba69a-20e4-42de-9cab-59c4f70f1406",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 49,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 215,
                "y": 155
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "99c8ba30-5f39-41f5-8da9-4164dcafb3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 49,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 236,
                "y": 104
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6fa47c8c-962b-4769-b58d-94515d37db2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 49,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 176,
                "y": 155
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "01d2b828-2042-439a-98c2-374d73f878e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 49,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 374,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "63bdfcb9-51f4-495f-b6ae-fefcd58aeded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 49,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 316,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4385258c-2ecb-43ae-81d3-a445af656e81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 49,
                "offset": -1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 155
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b72c5532-4975-40e6-b37d-05d44d6e50d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 49,
                "offset": -1,
                "shift": 18,
                "w": 18,
                "x": 296,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7aa7e96e-a8b2-4551-b058-5ec3bfb9cdd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 49,
                "offset": -1,
                "shift": 19,
                "w": 19,
                "x": 195,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "023c2dbf-8b6c-4d25-9d97-c8ed6161bf3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 49,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 276,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7cf642d0-8117-44d3-ac5d-ac37259c07ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 49,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 256,
                "y": 53
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0286bfb8-9f32-4b3d-8124-7cf10b371779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 49,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 54,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fc00f45e-6a88-40f2-bef1-ee221e35963d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 49,
                "offset": -1,
                "shift": 18,
                "w": 18,
                "x": 236,
                "y": 53
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "984fe190-66a4-47cd-99b1-08371364bfbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 49,
                "offset": -1,
                "shift": 20,
                "w": 20,
                "x": 25,
                "y": 53
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "729a5b81-94eb-480e-9f58-a924f91ccc08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 49,
                "offset": -3,
                "shift": 17,
                "w": 19,
                "x": 69,
                "y": 53
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "548b5ad5-cc29-4b46-8fe3-794a211251bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 49,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 184,
                "y": 155
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4c75c72b-55ec-48a4-a87e-f330060eb2a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 49,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 192,
                "y": 155
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "39e31ccb-6ae4-4221-a14b-b86742ade2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 49,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 470,
                "y": 53
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "61c7548f-d38b-48fa-957a-5244c556c6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 49,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 316,
                "y": 104
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b3dc16bc-d0f3-4d3e-a8b9-dfe5e334ef52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 49,
                "offset": 3,
                "shift": 19,
                "w": 16,
                "x": 489,
                "y": 53
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "530ee3a8-2575-4419-91a6-4d0159604a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 49,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 332,
                "y": 104
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8d6308c4-1f82-4d14-b8e0-2b64ca00178f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e3200e17-00c4-46c2-955e-b6ec85a18249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 443,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8e0e979e-7f4d-49dc-b2b1-45a007dc167b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 420,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3c42fd7f-b46f-4694-bcca-451bfd99531d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 49,
                "offset": 2,
                "shift": 22,
                "w": 21,
                "x": 397,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e4179708-84cb-484a-8cd2-a5bfc2d19302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 49,
                "offset": -1,
                "shift": 18,
                "w": 17,
                "x": 451,
                "y": 53
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b691c367-9785-46c2-8698-a825dc2aabe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 49,
                "offset": 2,
                "shift": 18,
                "w": 19,
                "x": 153,
                "y": 53
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2f7e5392-0622-4c13-909c-d51a18f16f7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 49,
                "offset": 2,
                "shift": 15,
                "w": 15,
                "x": 122,
                "y": 104
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "635ac904-16a6-4872-bdfd-b7749cbb61d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 49,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 20,
                "y": 104
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6119e6d4-fa12-4ff4-b1db-0e12a7e101fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 49,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 105,
                "y": 104
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5798fe11-3333-4ce7-a2a5-c07011bef4dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 49,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 413,
                "y": 53
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9ce31928-06d4-46f8-b219-7ad39d2254e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 49,
                "offset": -2,
                "shift": 17,
                "w": 23,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d68c43a8-b032-432f-a773-cf258507e635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 49,
                "offset": 2,
                "shift": 20,
                "w": 19,
                "x": 174,
                "y": 53
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "afd68791-2ebc-4fdc-aead-bf55d12cd681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 49,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 356,
                "y": 53
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a06a99c4-f12c-48bb-86dc-6481201480df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a17a66d2-5491-4e89-9bf6-d486f92ac3f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 21,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "309abfb0-7274-4562-af6b-2ac081e39cfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 49,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 216,
                "y": 53
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3b2c805c-2f52-4dc3-bdeb-63a5e5d31085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 49,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 394,
                "y": 53
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b1ca032d-641e-479d-a196-b92c8264d6af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 25,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a6377d71-97e2-4852-ae4a-de545d61d46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 49,
                "offset": 2,
                "shift": 19,
                "w": 20,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "408c0741-5649-447f-be1e-097fe58a9d13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 484,
                "y": 104
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dc44e540-c7cf-461f-88cb-bab32ea72f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 49,
                "offset": -1,
                "shift": 17,
                "w": 24,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d428e242-5188-4462-b9be-5a29b4b6bf3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 49,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 375,
                "y": 53
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "379f91cc-1dda-4a16-bd05-9a5b220f6fb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 49,
                "offset": 0,
                "shift": 16,
                "w": 19,
                "x": 90,
                "y": 53
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c12049da-5f9e-449a-a203-2cb66ca75d10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8ed54382-f905-4fab-83a7-afc042514257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 49,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 351,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ac2ed2fe-516d-4e82-ac70-285f105fa5d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 49,
                "offset": -1,
                "shift": 17,
                "w": 21,
                "x": 328,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ec3763f9-05f5-4c86-a735-24ff90ff31db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 49,
                "offset": 0,
                "shift": 21,
                "w": 24,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6d4648c2-c7ff-4037-b942-c3b86c0363a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 49,
                "offset": 0,
                "shift": 17,
                "w": 19,
                "x": 111,
                "y": 53
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5f56d9c1-f64d-4331-9197-31decd62244b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 49,
                "offset": 0,
                "shift": 20,
                "w": 22,
                "x": 281,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "14b21551-9f38-48b2-b2fe-d3510216365e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 49,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 132,
                "y": 53
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1d24bcbe-2d43-45db-a379-87dada2af3a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 49,
                "offset": 3,
                "shift": 22,
                "w": 15,
                "x": 71,
                "y": 104
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ba7fa6c8-48dd-40f2-a4bd-908b7f4fcbf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 49,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f91daa06-c3b7-469e-b6c8-1c007411f8dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 49,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 167,
                "y": 155
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8444ac80-497e-455c-937c-05cbe96a5854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 49,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 155
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "322e100d-4cc1-409d-abf0-3731fe531707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 49,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 454,
                "y": 104
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f7455040-f51f-42b0-ab80-1a49c525f9ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 49,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 424,
                "y": 104
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4cc83f87-ced1-4a65-91bd-a158581117c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 49,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 268,
                "y": 104
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "974be2d8-ef8c-45e3-9c4a-55956818a239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 49,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 439,
                "y": 104
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "00c9cec8-4ace-4c35-9864-af98fbfde18b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 49,
                "offset": -1,
                "shift": 9,
                "w": 12,
                "x": 30,
                "y": 155
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fee28d61-2bf4-444a-8067-45b7c4383451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 49,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 409,
                "y": 104
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2c0be03a-0498-40a6-b0b0-51ca0d5a44da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 49,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 394,
                "y": 104
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ee96f8b3-fc53-46b7-a177-0a0af6a790b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 49,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 200,
                "y": 155
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "42211178-c005-4554-913d-8ac350b78a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 49,
                "offset": -5,
                "shift": 10,
                "w": 14,
                "x": 172,
                "y": 104
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f481c511-3147-4f85-b30d-6be365aefdb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 49,
                "offset": 2,
                "shift": 13,
                "w": 13,
                "x": 379,
                "y": 104
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8558a8cc-4a37-443c-a5ac-070a2907f1f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 49,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 148,
                "y": 155
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "08869078-ffca-4942-bedd-fff71d9c412b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 336,
                "y": 53
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "437e181d-f74b-4710-916d-91a5fb31ea70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 49,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 86,
                "y": 155
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "22463b69-b254-4c57-b640-6781be8fd35a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 49,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 155
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7cb9d48c-057e-4ba3-8dc9-f985ba2eef20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 156,
                "y": 104
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c7991fad-698a-476c-a9d4-7030bdc73f6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 49,
                "offset": 2,
                "shift": 13,
                "w": 14,
                "x": 204,
                "y": 104
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a4c68e2d-8b5c-433b-9ca8-e75939bc28a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 49,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 126,
                "y": 155
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "50e21fd6-8a12-47da-be42-d828ebc4ea00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 49,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 100,
                "y": 155
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ef3315b2-56ea-44c5-9d0e-e1a4db10f364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 49,
                "offset": 0,
                "shift": 12,
                "w": 14,
                "x": 252,
                "y": 104
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e059dade-5b97-41d4-ab03-ea2710f3c2c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 49,
                "offset": 2,
                "shift": 15,
                "w": 14,
                "x": 284,
                "y": 104
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "aaf0b13f-0aa2-45ff-9de4-11a1216d4997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 49,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 16,
                "y": 155
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c44a5384-7471-463a-b7de-a8d7c9a37848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 49,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 37,
                "y": 104
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b153e4fd-d348-4a8a-8bd3-47d0c9efb71a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 49,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 364,
                "y": 104
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c93d0055-8b30-4fd3-b864-f75be769b503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 49,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 300,
                "y": 104
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2b566e95-255c-472c-a18b-0c0aaabc6dca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 49,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 88,
                "y": 104
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c6cf5625-38dc-4b01-9b6b-19877dd7925f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 49,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e30d97ca-a2d3-4423-a495-c822cf160d94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 49,
                "offset": 6,
                "shift": 15,
                "w": 11,
                "x": 113,
                "y": 155
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "38ee57c2-a8c7-4529-a75d-18240fa9f0c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 49,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 469,
                "y": 104
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c7b851fb-5696-4b91-8ce0-914d7a7b7b5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 49,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 432,
                "y": 53
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 32,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}