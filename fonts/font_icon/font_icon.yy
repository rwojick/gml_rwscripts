{
    "id": "2636ea85-c38c-4d99-9d9b-f32060c17599",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_icon",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Wingdings",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "37931de9-d6c7-44be-86cb-1fe20db61813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 21,
                "y": 22
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c934a66b-a8c9-4e26-a569-ac8348dd3fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 129,
                "y": 22
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "83ee0e64-5af1-4e60-95eb-2f7be922b80d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f5d07038-8199-47a3-ba78-d140d695b315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "71d546c3-01de-40f0-a6db-fe0af5e107fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6cf5b3d6-5664-4343-b35d-6b610d26db97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 148,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fef41ef0-d26a-4595-9f09-b883f9d1a102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c290c711-7c8a-4519-95b1-cfeb42a19665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 160,
                "y": 102
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "11b7bf4f-3674-4371-a087-5db8122c8f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e0c83b98-7a42-44f5-8e48-e9f9db808630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 82
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f7669ec3-fbd7-412e-9303-ac195e14ba27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 93,
                "y": 22
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ddac765e-e546-4b8d-91a1-74c5c1bfb020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 75,
                "y": 22
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4214dd9f-9831-4868-92b0-5996966d9712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fdfabea9-d75c-4d6a-9e52-8af9de0ad86c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6a942eda-d1f2-429c-979a-dc94364c4afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "db7cfe34-1cf2-44c5-8801-575bc5118ad6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "642938ec-42a4-4bda-a037-db4b90e23d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 57,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "38a8d2c0-e04b-4350-b534-16dd88bce6d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a6e375ea-a99f-4728-b200-f7f63a52fe73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3441fe06-8e76-41ac-b7e4-b81738009957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 102
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2a14bf4d-bf03-48df-b9a1-04e94b9c0fb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 82
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8b01cb39-1576-43e7-a1e2-09eaafa40346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 102
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "95d44797-f7c9-44ef-ae8a-9c2d71b6cef2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 79,
                "y": 102
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6788c461-d17c-486b-a688-0030f07364ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 217,
                "y": 22
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "48ff5b64-cd20-4ca4-8708-5b9134d1f0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 117,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7a1488e6-b756-41b1-8af2-7aeab927ab44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 165,
                "y": 22
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a7325f37-84da-48fe-8056-3c9e427e8340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "004ec75c-91f6-47df-82b8-7d6ab61b3094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 82
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2ea682c4-fcb7-4da4-861b-945b3eb2e1f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 82
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7ef475b9-93d8-4c39-98f6-42e6baa1d8f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "42788739-8c93-4e58-a163-d6ad8cb3db7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 82
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "69b2237e-1a7c-4fdf-9ad8-2a6cae9a91fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 85,
                "y": 42
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8c599a2f-5ba5-4b35-a9e9-95d5e6bcd029",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f3340181-c43f-43b2-a46c-73e60e2c6b09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 99,
                "y": 102
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4e99f1a1-5504-46c9-956c-4c47625b9552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ecbe228f-7dd5-4ad1-8fe8-66aed0512da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 102
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "39085604-496f-498e-93ae-9014802d722c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 102
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f0c094ec-54e6-4166-bc19-ce9f8114f4a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 238,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ecb8e9bb-8da3-4385-9f8d-a91195cf3c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "eab712be-cda1-4fb7-8347-838ad2f87822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 137,
                "y": 102
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c9c1141d-6521-431d-920e-0866ac6f5778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 145,
                "y": 102
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "da8c48ea-07da-4b55-be5e-3a8e321a09bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f13ba833-539e-4581-89d7-1d75594e0e82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 193,
                "y": 42
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "897a2656-f7e1-44be-988d-82222623f8e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 178,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fd6d16d9-92c1-4041-9491-6b5961a00eac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 163,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9fd70f8a-0fcb-403c-9ad7-ea8f4fcc2a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cdc76325-01ff-4b9f-8549-d2c896149106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 57,
                "y": 102
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "58fc9695-8b4f-4ddf-aac5-209210325919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 230,
                "y": 62
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bc488aa3-a4a6-4d2e-8ebb-69680ae0df43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 39,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6a861f2e-4051-4ff6-9567-77d8e324ccc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 82
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cb3b4ae0-8e1e-4764-9b5f-f6ea79ee7027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 82
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c50881ab-b5d3-48bd-a0bd-a507ce21d44b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 102
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7a15fd3b-f22b-4199-8eb0-661da13f1388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 142,
                "y": 82
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "72fa3667-db30-4893-b4c7-b6c983d5e720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 205,
                "y": 82
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e709fc2f-2534-4721-8c8a-d505820ebacf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 217,
                "y": 82
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "dc712e05-db59-4df3-8eb1-a3a0473284f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 229,
                "y": 82
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4a3d44b4-668a-4041-9ae4-f21cce793776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 102
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f2917db1-c0fc-4358-be39-629b60d44f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 168,
                "y": 82
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "bac9c732-2cd1-429d-a959-7cbe8644efae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 216,
                "y": 62
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6094b11a-79d5-4d43-bc5b-cafe011803d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 118,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3e5046f7-56b3-43f8-9537-bcb256ec6fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "272cf98e-8856-4061-b1ae-91dd142700cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8fd003be-29fe-4d39-8ad3-8e09d75b3e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "324facc6-4072-49a7-bed1-5bd3a7ab433b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 234,
                "y": 22
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "cdd2e06a-6af5-4689-871a-f4f4e209dca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 53,
                "y": 42
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8ccd70ee-80b6-44a6-ad8c-73cd1da9e1c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 147,
                "y": 22
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0ce980d1-e992-4ed8-a8a8-ad262131320c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 62
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9a94bb00-37a4-4968-b269-2812992fb928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 223,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ca9bcda2-e6a1-4752-a4cb-9043b4c55816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 133,
                "y": 42
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4d3eee7f-c7ab-4887-b720-c7e1768387a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 200,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b5ad0914-9d01-4f99-8cd4-b475aa70f266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 208,
                "y": 42
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "038a83e1-3ec1-4f22-8735-a8967e589bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 111,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d8094d7a-452a-4ab6-949a-af71037bd5fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 183,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9145262d-53d7-4b28-b8c2-7a11e40bfd45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 155,
                "y": 82
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e776797b-86c3-4b34-a799-09b84d9a94f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "399f9a5b-a5fa-4968-827d-0f3a2c4fd901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c47c7285-e35e-49b9-a9a5-9a760a445fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 241,
                "y": 82
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "96e2ef1a-6a3f-4f11-84f0-fb83e2dc7e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "75d12064-ccca-481c-9f69-b58ccacb4afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 181,
                "y": 82
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ad8798e4-6834-4dec-9d9e-02c8ae188566",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 202,
                "y": 62
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "756394f8-ce49-47c0-a904-cedc522bf8b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 104,
                "y": 62
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a742960e-e5a1-4443-b17e-8ec66dcdec4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 90,
                "y": 62
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b0822bd9-13bc-4460-9101-898bd293eeca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 132,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "794201bb-f745-4403-aabb-01e682cdc319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 153,
                "y": 102
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "14bf5d2a-dedd-41e2-b3ce-63626bfb8758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 193,
                "y": 82
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c03a45c5-d350-48ae-b56b-faa1845458a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 69,
                "y": 42
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "31ff4d07-8f73-47be-931a-fb945fe370fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 146,
                "y": 62
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7dfc5c20-7930-4fe5-818e-9a8cce2dba2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 128,
                "y": 102
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b3a4ca28-fb8d-406c-84f5-37a72e91cda1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3908a8dc-6676-42ea-af36-01a37fde027b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0ba2bfef-a08e-4a57-bc1a-77fe9b829b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 160,
                "y": 62
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "77774b72-56d9-48d4-8759-908d2b241e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 174,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6267dde1-1a5a-4b62-8d8d-0a6c5641223d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 188,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "067fb78f-bc25-49bf-88bb-fd0f18872539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 89,
                "y": 102
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "fa0939be-9d6c-44d0-b599-39554de71dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 109,
                "y": 102
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}