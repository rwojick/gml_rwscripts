{
    "id": "85bfa7c2-4191-4d03-be0b-53676fbd7ca2",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_main",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Press Start 2P",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3979c6b9-5ff5-4b8d-8807-f25d4acd1222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "05761d30-15c4-45e4-87b8-ea7eff431c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 184,
                "y": 182
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e4ec9529-4cb5-428a-8081-35e6032b816f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 6,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 12,
                "y": 200
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a8a72a28-ace5-4f8b-8406-bbd9addd6767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 38
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6d6c4dd4-c715-4095-87a1-8421d03b924e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 20
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b6235a0e-5be6-4dec-b03f-0400fffcfe7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 20
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e88931bf-4aa8-480c-83cf-3a1f002931fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "092cfb12-4f83-43ec-a51b-149a1b6de0a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 6,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 74,
                "y": 200
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ecc0aaa9-5f57-48aa-b1c1-f45ad061faa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 130,
                "y": 182
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "19c9f40f-3d1b-4482-b59c-2e078f4c0c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 80,
                "y": 182
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bb3bb304-3236-45fb-b0a1-9e341d0fae47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 164
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8cc2dd5b-64c7-4728-8a51-680ad33cab23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 186,
                "y": 164
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0ab6c661-335e-4696-a927-0edbddf1bba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 2,
                "shift": 16,
                "w": 6,
                "x": 176,
                "y": 182
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6915cdae-2e96-4755-92cd-0c2aa309e098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 8,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 162,
                "y": 182
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "76fe0098-3ccc-4b4e-8917-c90200746b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 36,
                "y": 200
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7c748fa1-5c94-46e7-9c8a-f4abdb9b2639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 110
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "212ef054-61bb-4e60-b201-e15872d9e72f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 128
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b9b7a2aa-2beb-4d52-b7c1-96fb91f11892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 172,
                "y": 146
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b0113b51-00f6-44ab-8a0c-3965c2bcfffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 128
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "af77299a-f3f0-49a1-9ee0-5ec173683d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 128
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b444ecb8-8c63-41a1-8cc5-5427a4bb6ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 110
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b2bcc79d-9086-4576-a55b-bd36a5ff0d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 128
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "00bd9966-5713-4130-a48d-bf38b879786a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 128
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0696f761-b964-45a8-981d-84dd5849150c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 128
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9550bd6e-646d-4a64-9455-dcbbb9f4a4b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f3f210d5-2f2f-4e0f-a06a-de591032551b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 128
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a6cf652e-aa62-4df8-aa1f-84fa0d8d655e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 42,
                "y": 200
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5c72c9d3-3801-4a6f-a0de-347e10d91d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 6,
                "x": 192,
                "y": 182
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "924a59ac-6c46-4647-a035-2a84396e40af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 18,
                "y": 182
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2acf8193-df1c-4683-9505-aba8845301df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 10,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 182
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3ec82cec-a33d-4a85-8d6d-c355045afb34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 42,
                "y": 182
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ce2444af-b2f5-43c5-845d-8d8382d98d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1c994ab4-870f-4f00-b59e-78fca2828d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 92
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8264d0e2-c38a-43f3-bb7d-83f6998e6098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 92
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9105bc63-f283-4d96-8f8f-83fbeb8d9f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 92
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e4221ef6-7564-4c1a-8463-3119b9bc1a5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 92
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3b32e6b1-cba8-453c-be42-bce50174214b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 92
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "26c8e93d-7f7f-43fc-b5d4-da711a20d48a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 92
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9132f3ff-e586-4410-8e17-3800700a2c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 92
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "87c41b64-f826-4dd2-972f-f9a33c9dd17d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 92
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "413ec0ac-f514-4ab3-bbd6-bd401c006f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 92
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "eada4623-82c7-45c9-8667-aa890fbabf1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 102,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d0a16bf8-c695-41c3-9df9-eddcc9153290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b0ed4ae6-912f-447e-887a-8581dccd5687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 110
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "33ebc27b-ab3b-430f-a236-02b3d0d5e46a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 88,
                "y": 146
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "90afc746-c071-4eaa-97d3-214932c7dbe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 110
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "595ce70f-5776-48df-8489-fe4acdba23ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 128
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d02b02a0-8cfb-41db-b311-687250503f9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 92
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "07b0fafc-2e3c-446a-8f63-d20d4176f4dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 110
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e7d3fbc9-1a97-4ad2-a2a9-671b58645a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 110
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "87298d26-a1a5-4b7d-ad87-657137a72460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 110
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c0bfa16f-dc0a-4b02-a939-bb5df68d40da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 110
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c1ffd67e-bbdf-476d-9006-2d619fc83f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 200,
                "y": 146
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1399db44-bba8-4446-8ec5-d1092bee96b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 110
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ebc02146-2067-406e-a88b-b1537c9f66aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 110
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1abb7560-6323-47e7-aa2c-70e2c62688e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 110
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cb54d59d-9545-4b27-97e1-78ad07e6a864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 110
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c97df335-3e92-4650-9d8a-94bd8df188f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 32,
                "y": 164
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0325e354-932a-4be5-bb2e-5119cce1db06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 92
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cccf79f0-fe2c-49f0-ac13-756301190bbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 90,
                "y": 182
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "93421458-df6f-4a87-84c2-edd0f0eee72c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 92
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0cb2ae39-c439-4b8a-ad8d-8fbc6f21eeac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 100,
                "y": 182
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5909af0b-b889-4395-a724-09c86d262b21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 4,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 48,
                "y": 200
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "03b3eac2-a39b-4ee9-b5d1-01fe36a6de3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "33b96789-9a5f-439e-ac88-a2f7c6722ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 4,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 110,
                "y": 200
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "38fec778-4f6a-4535-87b0-beb8aa2aae1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 128
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e253716a-77df-4522-acc5-182fb9a03931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 128
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e3bc51d9-005f-4ac0-a381-ca4af3ad5a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 128
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f381c122-c559-470f-b62e-43d58ab196d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 128
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b06017fa-78e2-4f3f-aac8-c4d8b47347c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 128
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5b928ac6-a444-488d-a191-2a65bc8c747c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 116,
                "y": 164
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "60fd2733-3055-43ae-b76f-047a50ffaeca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4fb1fade-3216-4e94-80c9-2da74a6c14b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 128
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "cb7fb8fb-6a35-43dc-b8fc-e7413f4958c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 230,
                "y": 146
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "79d9655b-e0ed-48f4-be2f-85455d79c065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 174,
                "y": 164
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "41fa2a9c-24ab-4503-ba2d-3274d6bd2bff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 92
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f8211c41-8dcd-4cf2-9fe5-68b49762037a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 88,
                "y": 164
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4ff3bdf5-82b9-4b06-8b7a-00de8a8d3fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "caefaca4-394a-4f36-885b-52e1e934e72c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c5bc01ea-0bee-42ca-804c-344be8ed1a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "aacc905f-5e0c-44fd-bf02-6a6779e5ea57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "255d35bb-5dfe-422b-a146-3c1a51ff395c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6ab23833-e3a3-4dc0-8de5-04d362ee07f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 46,
                "y": 164
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f7b15756-4d51-49c9-bfa1-ab18937a6c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 38
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e98efe3d-ee14-4358-a29c-580915fa134b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 60,
                "y": 164
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0fd05134-61d6-4758-b361-8095ec4d76ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 38
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3207b125-2fab-4be3-8994-eb7d04fb7c43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 102,
                "y": 164
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "888d0d48-44fc-4b0f-9660-5027b9819eaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 38
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4365cc46-bd4c-40ec-9711-8b1c82b3ab38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 38
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "68b7cefb-3655-4d5d-8062-e85e6df71b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cd1bca20-f800-430a-be5d-303c2730bfec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 38
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d5716d74-35e5-4ccf-826b-c3d8bc81c868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 120,
                "y": 182
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "67ac9802-9de5-429e-9dfe-2df41913031c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 30,
                "y": 200
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5dcd0773-2993-4f2f-b9a0-48f51c064e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 110,
                "y": 182
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7ebbcbb5-f7ca-4043-b2b2-4fc192f202e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 10,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 228,
                "y": 164
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "b9bb8933-d7b6-46dc-8594-0a40302f4760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 30,
                "y": 182
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "b04ec1cc-d192-425f-8f22-000c5d3b8f36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 0,
                "x": 116,
                "y": 200
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "c25f4af4-f194-4aba-84c1-9295f2cac04d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 14,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 200,
                "y": 182
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "e20982cd-a0af-4840-99bb-231871eaf0a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 38
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "77d9a2d0-44a5-45af-8ea9-b4b4de82b384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 38
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "f2abb17f-fcc4-4ec1-84fd-0b4be2fcfe63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 12,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 200,
                "y": 164
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "acb29c89-5055-4cfe-9017-585d26261054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 160,
                "y": 164
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "0c4c38dc-9144-4838-a97b-09b5a0378523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 14,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 24,
                "y": 200
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "528aab62-dd25-46d3-bf2e-60b4af3840fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 130,
                "y": 164
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "3f463472-b223-463d-a80c-62f4cd5aa57e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 2,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 92,
                "y": 200
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "234a8f79-9599-4ae0-b0d9-05feca824829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "9c61962c-52b3-4660-bd3d-5d7fa85294c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 8,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 208,
                "y": 182
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "677bbf74-2cf3-442f-bf41-8b80b90112b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 12,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 144,
                "y": 164
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "cd5c8f11-02d2-4461-bdc4-588cf68887fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 10,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 66,
                "y": 182
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "6fcfc49d-053c-4aec-8fea-b2257a223f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 8,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 148,
                "y": 182
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "b94a0b01-3b81-4fc1-afbb-3cf394fae2bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "5a76a641-0d9e-4d1e-8bd3-97064c32c741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 2,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 80,
                "y": 200
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "2d3bac37-abd0-48f3-8612-99e395d92fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 6,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 66,
                "y": 200
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "a82fd04f-15ba-4fcd-bd74-de0817a61ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 74,
                "y": 164
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "fa91a2eb-dc6c-4b02-b554-8f5b550a15ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 8,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 232,
                "y": 182
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "0849b968-1869-4607-96b8-8e213769e994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 8,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 242,
                "y": 182
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "9140c339-37a2-464b-9732-d55b2168be62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 4,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 104,
                "y": 200
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "1d3060f3-6fb2-455a-8da8-898f9fbc94d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "9f61a580-38ff-43b7-a269-5706e6371aa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 18,
                "y": 164
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "237681bf-8c28-436c-a72d-71558b6c0497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 10,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 60,
                "y": 200
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "238adfe2-4980-4785-806a-c2cb5009a096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 16,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 140,
                "y": 182
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "24e72db0-db73-4a1f-be3f-cae45ad0e064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 8,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 2,
                "y": 200
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "2311dded-bb5c-49b8-b314-045809c17611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 8,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 220,
                "y": 182
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "05f4c3f1-536e-49fc-930d-c2a1c67410b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 12,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 214,
                "y": 146
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "bdb78487-54f6-4935-82cc-7fee791c389b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 20
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "85d9662b-51f3-44a0-85ac-dff8cd34460c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 20
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "609082d4-6491-429a-b6fd-fda0c3106f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 20
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "fb301a70-dd92-4d5e-9a61-67c6d01d12b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 20
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "ea0c0cb6-3b2e-441d-87b5-dec974b0dfa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 20
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "4755791c-641c-4b58-9701-e6778a4f0087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 20
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "67301b7b-8518-4cd4-ae93-0b334172d772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 20
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "9e1246fc-7e0e-4fdd-aedb-9f6f574e16c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 20
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "cf5bfba9-055d-4059-8626-b56306ad6537",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "24a350df-b946-48e0-b2c4-07b6018f88f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 38
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "0fd6057b-2261-4923-bb97-7adea49b8c6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 38
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "ee84e054-6ccc-4b5a-ac51-06aec32e41f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "da45e3fe-3323-40e5-add1-86dca646b1af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "547f77fd-7ee1-4c16-91f0-10114cc4d7ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 74
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "1b91d61d-0061-43c8-bf11-41273d35d52a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 74
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "515352e6-825e-4408-b573-b8449a71ca95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 74
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "e96b9daf-a4ff-4ceb-83bc-22bff5846da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 130,
                "y": 146
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "7c90ad1e-3868-428d-b7a7-b7183a7f02de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 144,
                "y": 146
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "fae6d4a9-8689-4269-ad99-b2aefa35e433",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 158,
                "y": 146
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "61381844-58d0-494d-b7c4-605368a19ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 18,
                "y": 146
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "dc344fc8-edf9-4d0e-b58c-10b06b36ba1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 74
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "2563f5d0-b69f-4249-ab3d-ec4e2a4c71aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 74
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "61afffb2-28e7-459d-bd46-fb94dafd52e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 74
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "e08b6e18-2101-4658-83cc-c19af3887ecf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 74
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "e2b564d2-5436-4ad9-98b8-0876d3a2e8b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 74
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "5a71d9a7-d940-4feb-a698-04b062bcf823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 74
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "67cfdb57-6555-4b08-a7c6-76c1088d77d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 74
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "4aca9571-5a6d-4bd2-9ae9-3c5918cfbd56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 12,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 54,
                "y": 182
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "7e323853-fc5a-43bc-bd52-9371de8f9ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 74
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "3dee745f-70af-4674-acb9-4c6d7d3ba8e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 74
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "dcfc42df-0444-4102-9c15-6429d02cf8ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 74
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "3bcb5d73-a70b-4ae8-b952-93c759516732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 74
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "346e22c2-6cb6-4439-895d-3eb8bdcbf3b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "5f496056-ce4c-422c-a85d-6b14acab4b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 116,
                "y": 146
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "4e01689f-4762-46ed-823d-fced11b42269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 56
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "0224180f-0cbe-4317-8918-3e3bc9291365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 186,
                "y": 146
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "364ebdc7-00a2-455d-b915-f5ce88c2db24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 56
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "4c0527e0-b4f2-475a-aad1-5269b39f1000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 56
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "1ee72ea4-65a7-447b-8d29-bc021021c314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 56
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "c98292e8-f942-4547-a672-f1a93dc45ea2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 56
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "d9fc5cbe-01bd-4749-8997-9b349ab61aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 56
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "29629419-4d30-4d44-ba3f-7746383c3ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 56
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "4ca56d39-7060-4a52-b353-3a72a2796329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 56
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "eecaabc8-4a76-4948-9576-c596e5240e4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "71b1ceaa-0cdd-4ae6-8a78-8a08063be152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 56
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "c335aca7-8b4c-40d5-9000-2ec02df2e848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 56
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "81a7b7ca-afc4-4e29-86b0-5a7bc227f5da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 56
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "a905cfd5-3ece-4511-b45e-0a7d4250fa9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 56
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "db7a2647-0443-402a-953c-b9cc5505ff91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 74,
                "y": 146
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "453236c2-7460-414c-b0d4-7e81a7056786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 60,
                "y": 146
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "c874e2dc-7768-4f32-a1e0-971d6aeaa1a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 46,
                "y": 146
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "f44e7025-ff23-45ab-8e6b-7cd1303599b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 14,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 32,
                "y": 146
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "33192daf-1baf-4c9f-a68a-d83e539f45a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 56
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "ccffefb8-46e3-43f3-8d4a-eff3f53d3a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 38
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "152c47f2-ea09-4424-9771-61bc10dc7561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 92
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "3eb4a667-1c93-4630-bebf-122b20c6f10c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 20
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "c167539c-3488-409c-8c38-40025837e31e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 20
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "78eb70f8-1c23-4f20-aaf0-269183cb8f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 20
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "e0826263-3e56-42e9-82fd-7497aa8f2e5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 20
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "81bf277a-6301-4b44-9ad1-a39b30b69757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 12,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 214,
                "y": 164
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "cde47d1c-774d-490a-ab55-a1300853a7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "7ab4f83b-6dd2-4add-9442-0756bd0cd930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 38
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "6f9fb9d8-985e-487f-b685-78326d09e35c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 38
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "d8679fdc-84ee-4594-b699-bac373552dc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 128
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "940718a6-e377-4b0c-9e67-eb25a2f89cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 14,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 110
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "f947de75-3b91-46cc-91fe-ede0a4a88492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "86efe8b1-792c-420f-b81a-2263022300f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "24556c7d-b56c-4679-a689-cf9e5313819e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 216,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}