{
    "id": "9b741296-ecd1-4b30-b29c-dfac6dba21bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_master",
    "eventList": [
        {
            "id": "c8fe7297-ee0d-4d4e-94d8-939fa937deba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9b741296-ecd1-4b30-b29c-dfac6dba21bd"
        },
        {
            "id": "476b1561-39ea-425b-b452-b018fe777088",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9b741296-ecd1-4b30-b29c-dfac6dba21bd"
        },
        {
            "id": "c781396b-72cc-41da-bb54-e49e329c4baa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9b741296-ecd1-4b30-b29c-dfac6dba21bd"
        },
        {
            "id": "2958b74a-ceae-4798-b6b3-acc606846572",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "9b741296-ecd1-4b30-b29c-dfac6dba21bd"
        },
        {
            "id": "36090429-d2bf-4257-84ea-1b3cd988eb84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9b741296-ecd1-4b30-b29c-dfac6dba21bd"
        },
        {
            "id": "ca8674ba-4e76-45dd-be9d-48a9ef473076",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9b741296-ecd1-4b30-b29c-dfac6dba21bd"
        },
        {
            "id": "d90587b4-bcff-4f24-918e-696b3699edee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 7,
            "m_owner": "9b741296-ecd1-4b30-b29c-dfac6dba21bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "0c2fa3df-5ac6-4786-9e52-9e2c96318b3f",
    "visible": true
}