{
    "id": "0c2fa3df-5ac6-4786-9e52-9e2c96318b3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_system",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8f79d66f-2e52-4a51-ac43-d8b2345f86e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2fa3df-5ac6-4786-9e52-9e2c96318b3f",
            "compositeImage": {
                "id": "8782cba0-0125-4f80-a308-655c3e2d147b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f79d66f-2e52-4a51-ac43-d8b2345f86e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2649a384-c101-4d6c-8d09-1582d6bb1140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f79d66f-2e52-4a51-ac43-d8b2345f86e4",
                    "LayerId": "fef6bc96-ab5e-477d-9ce3-eebd4a358567"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fef6bc96-ab5e-477d-9ce3-eebd4a358567",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c2fa3df-5ac6-4786-9e52-9e2c96318b3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}