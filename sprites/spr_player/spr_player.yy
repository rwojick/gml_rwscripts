{
    "id": "6aafb9da-9f4b-4eee-ac23-9e545bc3170a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5da3886d-75af-4c51-b40c-168c1d48efd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aafb9da-9f4b-4eee-ac23-9e545bc3170a",
            "compositeImage": {
                "id": "8e92e360-d12b-4fde-b411-dc45f372b5f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5da3886d-75af-4c51-b40c-168c1d48efd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e0275c-052b-4331-b15f-d4785ab12290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5da3886d-75af-4c51-b40c-168c1d48efd1",
                    "LayerId": "d704e770-d05b-4eac-9731-22e6eca96dda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d704e770-d05b-4eac-9731-22e6eca96dda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6aafb9da-9f4b-4eee-ac23-9e545bc3170a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}