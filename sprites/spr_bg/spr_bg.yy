{
    "id": "61d753ab-9db6-436a-b8b2-242537234731",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a964f17c-d193-4278-9372-cbd2e1abbf45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61d753ab-9db6-436a-b8b2-242537234731",
            "compositeImage": {
                "id": "b58350fb-d709-4d2b-95f7-7d884aa6c21d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a964f17c-d193-4278-9372-cbd2e1abbf45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843dc5e8-7ef0-4927-aea0-dfab9b23329a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a964f17c-d193-4278-9372-cbd2e1abbf45",
                    "LayerId": "eeb22c5c-2e29-409b-bce0-e61ff495b72f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eeb22c5c-2e29-409b-bce0-e61ff495b72f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61d753ab-9db6-436a-b8b2-242537234731",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}