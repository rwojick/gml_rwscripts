{
    "id": "59fcb272-94d1-4406-b486-b57754bd3b9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "font_main_old",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f8f7cb5a-95a2-461e-a1bb-83bde607e1a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "e334bb0b-7292-4534-9fa0-5dc158d2d8ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8f7cb5a-95a2-461e-a1bb-83bde607e1a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4b1bf3-51da-4d7e-9c36-9569a02e009f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8f7cb5a-95a2-461e-a1bb-83bde607e1a9",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "842a301b-aafd-4b66-8ce3-6344bcff2fe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "2bf13275-64d7-4ddb-9479-43da7b2fccb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "842a301b-aafd-4b66-8ce3-6344bcff2fe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b5a9175-eff9-409c-a8fb-2023d051543c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "842a301b-aafd-4b66-8ce3-6344bcff2fe7",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "94368ce6-63e7-4667-93be-5b6a0de654cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "21930467-f1f1-4ccc-ac29-69fad233cf2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94368ce6-63e7-4667-93be-5b6a0de654cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1cabcdb-b97c-4b75-a228-985e2c6b6d49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94368ce6-63e7-4667-93be-5b6a0de654cd",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "0092d9f4-c249-43e7-b803-d8393842d24f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "c01c6a77-5ed0-417d-899b-8d774755f2d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0092d9f4-c249-43e7-b803-d8393842d24f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "510767c9-f188-45fa-b1d7-53ba36d2a8dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0092d9f4-c249-43e7-b803-d8393842d24f",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "fcb3ec47-dba0-4c77-b1f6-286659e7c8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "6b3d9327-21e7-47df-a7ae-ac900b002914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcb3ec47-dba0-4c77-b1f6-286659e7c8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b437625d-6f84-464b-b780-bb7902ab36c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcb3ec47-dba0-4c77-b1f6-286659e7c8d4",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "546ff36c-4edf-4499-9795-bba85e38fbf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "c2a80f8e-d179-4940-940a-7092714d2c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "546ff36c-4edf-4499-9795-bba85e38fbf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e17dd43-66a1-406d-9eb2-73ce84f8978f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "546ff36c-4edf-4499-9795-bba85e38fbf0",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "4e03f1f8-3981-473d-b00f-805c6ea638e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "618cd24c-241b-4079-b77e-e4f96abfff39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e03f1f8-3981-473d-b00f-805c6ea638e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f1ba59c-ddea-4010-ad7b-535352559ad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e03f1f8-3981-473d-b00f-805c6ea638e5",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "839d5e06-8722-47be-a11c-ad906cba36e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "4b1b05d5-ce0c-4b40-9408-5f8afaa0e403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "839d5e06-8722-47be-a11c-ad906cba36e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d53588d-1c62-4287-92d1-7ad6ca177f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "839d5e06-8722-47be-a11c-ad906cba36e6",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "1f0d302a-9e3d-4746-9705-3aee23a88ce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "01c85105-d865-4ba3-a182-e28766d736ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f0d302a-9e3d-4746-9705-3aee23a88ce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6760fa8-2ded-4588-be4f-139d3d6d261d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f0d302a-9e3d-4746-9705-3aee23a88ce9",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "4cfb572e-5e3c-4c7c-bf37-cca71205ffb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "19fb3783-2e9a-4a5b-a0b3-0eb2d36f5b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cfb572e-5e3c-4c7c-bf37-cca71205ffb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a7214f-56d9-41cb-99e3-2484d098f428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cfb572e-5e3c-4c7c-bf37-cca71205ffb6",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "42cb2a27-6292-4793-ae28-1e5265bfe847",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "d564c1e4-7972-4292-bc71-09d3a21a45b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42cb2a27-6292-4793-ae28-1e5265bfe847",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc4abff-7863-4cc1-833f-dc8953edacc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42cb2a27-6292-4793-ae28-1e5265bfe847",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "500f6c85-58cf-473f-949e-6da4dc44abd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "58aff8e5-d509-45cf-80ad-541da224987f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "500f6c85-58cf-473f-949e-6da4dc44abd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75336d14-6de5-42af-836b-b5c714676d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "500f6c85-58cf-473f-949e-6da4dc44abd0",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "a850cdcd-eee2-4927-8b9b-f35b00a5deac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "bca946bb-136a-422e-9144-aaf3779d8b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a850cdcd-eee2-4927-8b9b-f35b00a5deac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7be17a8-28c6-4478-a920-1df6279e09e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a850cdcd-eee2-4927-8b9b-f35b00a5deac",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "4f1b7a09-24df-4bc9-92b6-f52edee169b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "f2738165-07f1-4f56-93a6-3abb82e7d9d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f1b7a09-24df-4bc9-92b6-f52edee169b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c60967-53c0-41bb-8790-b699c5aad198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1b7a09-24df-4bc9-92b6-f52edee169b5",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "6e62eaf2-96de-4479-ab87-01f41865780c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "4b6f61a2-017f-49ef-b0e8-157e5421c7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e62eaf2-96de-4479-ab87-01f41865780c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56161d31-6946-48ab-a795-1eaacdc70ee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e62eaf2-96de-4479-ab87-01f41865780c",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "533440d3-510d-48b7-ace8-5570e60c2491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "c98baa98-f808-4e69-a3e3-f2568a7fff63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "533440d3-510d-48b7-ace8-5570e60c2491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83a1ef31-b371-47d2-9a9c-c47192134f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "533440d3-510d-48b7-ace8-5570e60c2491",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "c13299ff-1717-476b-93f1-7ba7b067a643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "f40bb4a0-a46c-4894-9add-921bad0f4061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c13299ff-1717-476b-93f1-7ba7b067a643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41f18690-2dc5-4591-a53e-59b99e4fedb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c13299ff-1717-476b-93f1-7ba7b067a643",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "c688b974-aaaa-41fa-a90f-f91a59d1d783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "56a479f8-7758-494d-ae7f-ec5374bfa84a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c688b974-aaaa-41fa-a90f-f91a59d1d783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5263ea44-835d-4127-bad2-43566c78eafc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c688b974-aaaa-41fa-a90f-f91a59d1d783",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "8fd21d99-4f31-4976-83a8-d5fc709a3a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "990664b8-87e7-43aa-af96-464948767246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd21d99-4f31-4976-83a8-d5fc709a3a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b025ca-a9ff-49f6-9b30-6462ced187cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd21d99-4f31-4976-83a8-d5fc709a3a07",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "aaff00d2-261e-4e8f-92d7-972ddb5f474a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "4ebb88bd-60d9-4bdd-88ab-4a5a61789435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaff00d2-261e-4e8f-92d7-972ddb5f474a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6083d34-5d6c-4a0e-a5de-e3ef80668ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaff00d2-261e-4e8f-92d7-972ddb5f474a",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        },
        {
            "id": "27341556-b4ee-445a-8e12-5764f2d6adf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "compositeImage": {
                "id": "3229a036-b1ac-42cf-9d64-87dd5a6ff3da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27341556-b4ee-445a-8e12-5764f2d6adf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91af6a1e-7e89-4ea6-b316-dc29152b04dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27341556-b4ee-445a-8e12-5764f2d6adf0",
                    "LayerId": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 8,
    "layers": [
        {
            "id": "7ccef3b2-8c52-40b5-8601-b278d7ef5e6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59fcb272-94d1-4406-b486-b57754bd3b9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}